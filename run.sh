#!/bin/sh

set -Eeo pipefail

script_dir=$(dirname $(realpath "$0"))

benchmark() {
    meta=`$LUAJIT $script_dir/slpm.lua build -q --dry-run tools/lua_host`
    cmd='local d=require("api").public.json_decode;print("Lua::", d(io.read())[1]["lua_version"])'
    echo $meta | $LUAJIT -e "$cmd" > $script_dir/doc/benchmark.inc
    echo >> $script_dir/doc/benchmark.inc
    echo "|===" >> $script_dir/doc/benchmark.inc
    echo "|Mode |File size |RSS |Run time" >> $script_dir/doc/benchmark.inc
    echo >> $script_dir/doc/benchmark.inc

    $LUAJIT $script_dir/slpm.lua run -m fast tools/lua_host -- test/primes.lua >> doc/benchmark.inc
    $LUAJIT $script_dir/slpm.lua run -m small tools/lua_host -- test/primes.lua >> doc/benchmark.inc
    $LUAJIT $script_dir/slpm.lua run -m safe tools/lua_host -- test/primes.lua >> doc/benchmark.inc
    $LUAJIT $script_dir/slpm.lua run -m debug tools/lua_host -- test/primes.lua >> doc/benchmark.inc
    echo "|===" >> $script_dir/doc/benchmark.inc
}

test_target_info() {
    $LUAJIT $script_dir/test.lua target-info
}

test_lua() {
    $LUAJIT $script_dir/test.lua lua
}

test_luajit() {
    $LUAJIT $script_dir/test.lua luajit
}

test_openssl() {
    $LUAJIT $script_dir/test.lua openssl
}

test_curl() {
    $LUAJIT $script_dir/test.lua curl
}

test_procstat() {
    $LUAJIT $script_dir/test.lua procstat
}

test_example_curl() {
    $LUAJIT $script_dir/test.lua example-curl
}

test_all() {
    $LUAJIT $script_dir/test.lua
}

run_all() {
    test_all
    benchmark
}

if [ "_$1" = "_" ]; then
    run_all
else
    "$@"
fi
