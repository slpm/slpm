local build

name = "target_info"
description = "Tool for getting target information"

versions = {
  {
    version = "0.0.1",
    build = function(cfg, api)
      return build(cfg, api)
    end
  }
}

build = function(cfg, api)
  local bin_main_name = string.format(
    "%s-%s-%s", cfg.pkg.name, cfg.args.mode, cfg.pkg.version)
  local options = {
    bin = {
      main = {
        name = bin_main_name,
        object = api.path.join(cfg.prefix.bindir, bin_main_name),
      }
    },
    prefix = cfg.prefix,
    mode = cfg.args.mode,
    target = cfg.args.target,
  }

  return api.zig_build(cfg, string.format("-Doptions=%q", api.json_encode(options)))
end
