const std = @import("std");
const CrossTarget = std.zig.CrossTarget;
const Target = std.Target;

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = &arena.allocator;

    const options = @import("build_options");
    const target_str = options.target;
    const target = try CrossTarget.parse(.{ .arch_os_abi = target_str });
    const target_info = try std.zig.system.NativeTargetInfo.detect(
        allocator,
        target,
    );
    const resolved_target = target_info.target;
    const arch = resolved_target.cpu.arch;
    const bits = arch.ptrBitWidth();
    const os = resolved_target.os.tag;

    const stdout = std.io.getStdOut().writer();
    try stdout.print("{{\n", .{});
    try stdout.print("  \"arch\": \"{s}\",\n", .{@tagName(arch)});
    try stdout.print("  \"bits\": {},\n", .{bits});
    try stdout.print("  \"os\": \"{s}\",\n", .{@tagName(os)});
    try stdout.print("  \"features\": [\n", .{});

    for (arch.allFeaturesList()) |feature, index_usize| {
        const index = @intCast(Target.Cpu.Feature.Set.Index, index_usize);
        const is_enabled = resolved_target.cpu.features.isEnabled(index);
        const feature_name = feature.llvm_name;
        const description = feature.description;

        try stdout.print("    {{\n      \"name\": \"{s}\",\n", .{feature_name});
        try stdout.print("      \"enabled\": {},\n", .{is_enabled});
        try stdout.print("      \"description\": \"{s}\"\n    }},\n", .{description});
    }
    try stdout.print("  ]\n}}\n", .{});
}
