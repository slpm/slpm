local build

name = "lua_host"
description = "Lua host to run its scripts"

versions = {
  {
    version = "0.0.1",
    build = function(cfg, api)
      return build(cfg, api)
    end,
    dependencies = {
      { name = "native/lua", version = "5.4.3" },
      { name = "native/procstat", version = "0.0.1" },
    }
  }
}

build = function(cfg, api)
  local bin_main_name = string.format(
    "%s-%s-%s", cfg.pkg.name, cfg.args.mode, cfg.pkg.version)
  local dep_opts = cfg.pkg.dep_opts
  local options = {
    bin = {
      main = {
        name = bin_main_name,
        object = api.path.join(cfg.prefix.bindir, bin_main_name),
      }
    },
    prefix = cfg.prefix,
    dep_opts = dep_opts,
    mode = cfg.args.mode,
    target = cfg.args.target,
  }

  api.zig_build(cfg, string.format("-Doptions=%q", api.json_encode(options)))

  local lua_version
  for k, opt in ipairs(dep_opts.lib) do
    if opt["native/lua"] then
      lua_version = opt["native/lua"].version
    end
  end

  return {
    bin = options.bin,
    lua_version = lua_version,
  }
end
