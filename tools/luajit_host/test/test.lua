local function add(a, b)
  return a + b
end

assert(add(2, 3) == 5)
