const std = @import("std");
const procstat = @import("native/procstat");
const lua = @cImport({
    @cInclude("lua.h");
    @cInclude("lualib.h");
    @cInclude("lauxlib.h");
});

const assert = std.debug.assert;

const Allocator = std.mem.Allocator;
const Str = []const u8;
const Timer = std.time.Timer;

export fn traceback(L: ?*lua.lua_State) c_int {
    // Non-string error object? Try metamethod.
    if (lua.lua_isstring(L, 1) == 0) {
        if (lua.lua_isnoneornil(L, 1) or
            (lua.luaL_callmeta(L, 1, "__tostring") == 0) or
            (lua.lua_isstring(L, -1) == 0))
        {
            // Return non-string error object.
            return 1;
        }
        // Replace object by result of __tostring metamethod.
        lua.lua_remove(L, 1);
    }
    lua.luaL_traceback(L, L, lua.lua_tolstring(L, 1, null), 1);
    return 1;
}

fn lua_assert(L: ?*lua.lua_State, ret: c_int) void {
    if (ret != lua.LUA_OK) {
        std.debug.print("{s}\n", .{lua.lua_tolstring(L, -1, null)});
        lua.lua_pop(L, 1);
        std.os.exit(1);
    }
}

fn run(L: ?*lua.lua_State, allocator: *Allocator, file_name: Str) !void {
    const self_exe_file = try std.fs.openSelfExe(.{});
    defer self_exe_file.close();
    const stat = try self_exe_file.stat();

    const name = try std.cstr.addNullByte(allocator, file_name);
    lua_assert(L, lua.luaL_loadfile(L, name));

    const base = lua.lua_gettop(L); // function index
    lua.lua_pushcfunction(L, traceback); // push traceback function
    lua.lua_insert(L, base); // put it under chunk and args

    var timer = try Timer.start();
    const start = timer.lap();
    const status = lua.lua_pcall(L, 0, 0, base);
    const end = timer.read();

    lua.lua_remove(L, base); // remove traceback function
    lua_assert(L, status);

    const options = @import("build_options");

    const stdout = std.io.getStdOut().writer();
    try stdout.print("|{s}\n|{:.2}\n|{:.2}\n|{}\n", .{
        options.mode,
        std.fmt.fmtIntSizeBin(stat.size),
        std.fmt.fmtIntSizeBin(procstat.getPeakRSS()),
        std.fmt.fmtDuration(end - start),
    });
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = &arena.allocator;

    var L = lua.luaL_newstate();
    defer lua.lua_close(L);

    lua.luaL_openlibs(L);

    var args_it = std.process.args();
    assert(args_it.skip());

    while (args_it.next(allocator)) |arg| {
        try run(L, allocator, try arg);
    }
}
