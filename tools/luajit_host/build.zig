pub fn build(b: *Builder) !void {
    const opts = try getOptions(b);

    const app_exe = b.addExecutable(getBinName("main", opts), "app.zig");
    addIncludeDirs(app_exe, opts);
    addObjectFiles(app_exe, opts);
    addPackagePaths(app_exe, opts);
    setBuildMode(app_exe, opts);
    setBinOutputDir(app_exe, opts);

    const exe_options = b.addOptions();
    app_exe.addOptions("build_options", exe_options);
    exe_options.addOption(Mode, "mode", opts.mode);

    app_exe.linkLibC();
    app_exe.linkSystemLibrary("unwind");
    if (opts.os_tag == .windows) {
        app_exe.linkSystemLibrary("psapi");
    }

    app_exe.setTarget(opts.target);
    app_exe.enable_qemu = true;
    app_exe.enable_wine = true;
    app_exe.enable_darling = true;
    app_exe.install();

    const run_cmd = app_exe.run();
    run_cmd.step.dependOn(b.getInstallStep());
    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);
}

// BEGIN GENERATED CONTENT
const std = @import("std");

const Builder = std.build.Builder;
const CrossTarget = std.zig.CrossTarget;
const LibExeObjStep = std.build.LibExeObjStep;
const Mode = std.builtin.Mode;
const ObjectMap = std.json.ObjectMap;
const Str = []const u8;
const Target = std.Target;

const Options = struct {
    bindir: ?Str,
    cpu_arch: Target.Cpu.Arch,
    cpu_bits: usize,
    cpu_features: Target.Cpu.Feature.Set,
    libdir: ?Str,
    mode: Mode,
    obj: ObjectMap,
    origin: ?Str,
    os_tag: Target.Os.Tag,
    target: CrossTarget,
};

fn addCSourceFiles(
    b: *Builder,
    artifact: *LibExeObjStep,
    files: []const Str,
    flags: []const Str,
    opts: Options,
) void {
    for (files) |c_file| {
        artifact.addCSourceFile(normalized(b, opts.origin, c_file), flags);
    }
}

fn addIncludeDirs(artifact: *LibExeObjStep, opts: Options) void {
    const add = (struct {
        artifact: *LibExeObjStep,
        fn add(self: @This(), obj: ObjectMap) void {
            if (obj.get("includedir")) |dir| {
                self.artifact.addIncludeDir(dir.String);
            }
        }
    }{ .artifact = artifact }).add;

    if (opts.obj.get("dep_opts")) |dep_opts| {
        if (dep_opts.Object.get("prefix")) |prefix| {
            for (prefix.Array.items) |v| {
                add(v.Object);
            }
        }
    }

    if (opts.obj.get("prefix")) |prefix| {
        add(prefix.Object);
    }
}

fn addObjectFiles(artifact: *LibExeObjStep, opts: Options) void {
    const add = (struct {
        artifact: *LibExeObjStep,
        fn add(self: @This(), obj: ObjectMap) void {
            var it = obj.iterator();
            while (it.next()) |raw_entry| {
                const entry = raw_entry.value_ptr.*;
                if (entry.Object.get("object")) |object| {
                    self.artifact.addObjectFile(object.String);
                }
            }
        }
    }{ .artifact = artifact }).add;

    if (opts.obj.get("dep_opts")) |dep_opts| {
        if (dep_opts.Object.get("lib")) |lib| {
            for (lib.Array.items) |v| {
                add(v.Object);
            }
        }
    }

    if (opts.obj.get("lib")) |lib| {
        add(lib.Object);
    }
}

fn addPackagePaths(artifact: *LibExeObjStep, opts: Options) void {
    const add = (struct {
        artifact: *LibExeObjStep,
        fn add(self: @This(), obj: ObjectMap) void {
            var it = obj.iterator();
            while (it.next()) |raw_entry| {
                const key = raw_entry.key_ptr.*;
                const entry = raw_entry.value_ptr.*;
                if (entry.Object.get("zigpkg")) |object| {
                    self.artifact.addPackagePath(key, object.String);
                }
            }
        }
    }{ .artifact = artifact }).add;

    if (opts.obj.get("dep_opts")) |dep_opts| {
        if (dep_opts.Object.get("lib")) |lib| {
            for (lib.Array.items) |v| {
                add(v.Object);
            }
        }
    }

    if (opts.obj.get("lib")) |lib| {
        add(lib.Object);
    }
}

fn getBinName(selector: Str, opts: Options) Str {
    if (opts.obj.get("bin")) |binobj| {
        if (binobj.Object.get(selector)) |main| {
            if (main.Object.get("name")) |v| {
                return v.String;
            }
        }
    }
    return "bin";
}

fn getLibName(selector: Str, opts: Options) Str {
    if (opts.obj.get("lib")) |libobj| {
        if (libobj.Object.get(selector)) |main| {
            if (main.Object.get("name")) |v| {
                return v.String;
            }
        }
    }
    return "lib";
}

fn getOptions(b: *Builder) !Options {
    var target = b.standardTargetOptions(.{});
    var mode = b.standardReleaseOptions();

    var opt_origin: ?Str = null;
    var opt_libdir: ?Str = null;
    var opt_bindir: ?Str = null;

    var options_obj = std.json.ObjectMap.init(b.allocator);
    const opt_options = b.option(
        Str,
        "options",
        "Project options in JSON format",
    );
    if (opt_options) |text| {
        var p = std.json.Parser.init(b.allocator, false);
        defer p.deinit();

        options_obj = (try p.parse(text)).root.Object;
        if (options_obj.get("origin")) |origin| {
            opt_origin = origin.String;
        }
        if (options_obj.get("prefix")) |v| {
            if (v.Object.get("libdir")) |libdir| {
                opt_libdir = libdir.String;
            }
            if (v.Object.get("bindir")) |bindir| {
                opt_bindir = bindir.String;
            }
        }
    }

    if (options_obj.get("target")) |v| {
        target = try CrossTarget.parse(.{ .arch_os_abi = v.String });
    }

    if (options_obj.get("mode")) |v| {
        mode = if (std.mem.eql(u8, v.String, "safe"))
            Mode.ReleaseSafe
        else if (std.mem.eql(u8, v.String, "fast"))
            Mode.ReleaseFast
        else if (std.mem.eql(u8, v.String, "small"))
            Mode.ReleaseSmall
        else
            Mode.Debug;
    }

    const target_info = try std.zig.system.NativeTargetInfo.detect(
        b.allocator,
        target,
    );
    const resolved_target = target_info.target;
    const arch = resolved_target.cpu.arch;
    const features = resolved_target.cpu.features;
    const os_tag = resolved_target.os.tag;

    return Options{
        .bindir = opt_bindir,
        .cpu_arch = arch,
        .cpu_bits = arch.ptrBitWidth(),
        .cpu_features = features,
        .libdir = opt_libdir,
        .mode = mode,
        .obj = options_obj,
        .origin = opt_origin,
        .os_tag = os_tag,
        .target = target,
    };
}

fn normalized(b: *Builder, opt_prefix: ?Str, path: Str) Str {
    if (opt_prefix) |prefix| {
        return b.fmt("{s}/{s}", .{ prefix, path });
    } else {
        return path;
    }
}

fn setBuildMode(artifact: *LibExeObjStep, opts: Options) void {
    artifact.setBuildMode(opts.mode);
    if (opts.mode == .ReleaseSmall) {
        artifact.strip = true;
    }
}

fn setLibOutputDir(artifact: *LibExeObjStep, opts: Options) void {
    if (opts.libdir) |libdir| {
        artifact.setOutputDir(libdir);
    }
}

fn setBinOutputDir(artifact: *LibExeObjStep, opts: Options) void {
    if (opts.bindir) |bindir| {
        artifact.setOutputDir(bindir);
    }
}
// END GENERATED CONTENT
