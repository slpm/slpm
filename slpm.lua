#!/usr/bin/env luajit

local libc = require "src.luajit.libc"

local api_ns = require "api"
local api = api_ns.public
local argparse = require "src.lua.argparse"
local path = require "src.lua.path"

local root = libc.cwd()
local build
local target_info = {}

local function get_target_info(target)
  if not target_info[target] then
    local target_info_json = build({
        run = true,
        no_echo = true,
        pkg = "tools/target_info",
        target = target,
    })
    target_info[target] = api.json_decode(target_info_json[1])
  end
  return target_info[target]
end
api.get_target_info = get_target_info

local function get_lib_object(cfg, name)
  local info = get_target_info(cfg.args.target)
  local os_name = info.os
  local libfile = string.format("lib%s.a", name)
  if os_name == "windows" then
    libfile = string.format("%s.lib", name)
  end

  return api.path.join(cfg.prefix.libdir, libfile)
end
api.get_lib_object = get_lib_object

local function add_opt(dep_opts, k, v)
  if not dep_opts[k] then
    dep_opts[k] = {}
  end
  table.insert(dep_opts[k], v)
end

local function build_deps(dependencies, args)
  local dep_opts = {}
  if dependencies then
    for _, dep in ipairs(dependencies) do
      local dep_args = {
          build = true,
          dry_run = args.dry_run,
          mode = args.mode,
          pkg = "modules/" .. dep.name,
          pkgver = dep.version,
          target = args.target,
      }
      local options = build(dep_args)
      for _, opts in ipairs(options) do
        for k, v in pairs(opts) do
          add_opt(dep_opts, k, v)
        end
      end
    end
  end
  return dep_opts
end

local function build_pkg(pkg, args)
  local prefix = path.join(path.tmpdir(), "slpm-build", args.target)

  local cfg = {
    prefix = {
      bindir = path.join(prefix, "bin"),
      dir = prefix,
      includedir = path.join(prefix, "include", pkg.name .. "-" .. pkg.version),
      zigpkgdir = path.join(prefix, "zigpkg"),
      libdir = path.join(prefix, "lib"),
    },
    pkg = {
      dep_opts = build_deps(pkg.dependencies, args),
      dev_dep_opts = build_deps(pkg.dev_dependencies, "native"),
      dir = pkg.dir,
      name = pkg.name,
      version = pkg.version,
    },
    args = args,
  }

  return pkg.build(cfg, api)
end

build = function(args)
  local module_path = path.join(root, args.pkg)
  dofile(path.join(module_path, "manifest.lua"))

  local options = {}
  for _, pkg in pairs(versions) do
    if not args.pkgver or pkg.version == args.pkgver then
      pkg.dir = module_path
      pkg.name = name
      local opts = build_pkg(pkg, args)
      table.insert(options, opts)
    end
  end

  return options
end

local function add_common_options(parser)
  parser:argument "pkg"
    :description "Sets the package path."
  parser:option "--pkgver"
    :description "Sets the package version to build."
  parser:option "-t" "--target"
    :description "Sets the target architecture."
    :default "native"
  parser:option "-m" "--mode"
    :description "Sets the compilation mode."
    :default "debug"
    :choices {"safe", "fast", "small", "debug"}
  parser:flag("-v --verbose", "Sets a verbosity level.")
    :count "0-2"
    :target "verbosity"
end

local parser = argparse(arg[0], "Stage 1 SLPM manager.")

local build_cmd = parser:command "build"
  :summary "Build a package."
add_common_options(build_cmd)
build_cmd:flag "--dry-run"
  :description "Runs the commands without executing them."
build_cmd:flag "-q" "--query"
  :description "Queries the package metadata."

local test_cmd = parser:command "test"
  :summary "Test a package."
add_common_options(test_cmd)

local run_cmd = parser:command "run"
  :summary "Run a package."
  :description [[
Run a package with the optional arguments (after '--' delimiter).]]
add_common_options(run_cmd)
run_cmd:flag "--no-echo"
  :description "Doesn't echo the stdout."

local ext_args = {}
local have_ext = false
for i, v in ipairs(arg) do
  if v == "--" then
    have_ext = true
  elseif have_ext then
    table.insert(ext_args, v)
    arg[i] = nil
  end
end

local args = parser:parse()
if have_ext then
  args.ext = ext_args
end
api_ns.private.set_verbosity(args.verbosity)

local result = build(args)
if args.query then
  print(api.json_encode(result))
end
