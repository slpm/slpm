#!/bin/sh

set -Eeuo pipefail

script_dir=$(dirname $(realpath "$0"))

lead='^\/\/ BEGIN GENERATED CONTENT$'
tail='^\/\/ END GENERATED CONTENT$'
root=$(dirname $script_dir)

find $root -name build.zig -print0 | while IFS= read -r -d '' file; do
    echo $file
    output=$(sed -e "/$lead/,/$tail/{ /$lead/{p; r $script_dir/shared.zig
        }; /$tail/p; d }" $file)
    echo "$output" > $file
done
