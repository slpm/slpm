local ffi = require "ffi"
local libarchive = require "src.luajit.libarchive"
local libc = require "src.luajit.libc"
local libcrypto = require "src.luajit.libcrypto"
local libcurl = require "src.luajit.libcurl"

local json = require "src.lua.json"
local path = require "src.lua.path"

local verbosity = 0

local function set_verbosity(new_verbosity)
  verbosity = new_verbosity
end

local function eprint(fmt, ...)
  if fmt and verbosity > 0 then
    io.stderr:write(string.format(fmt, unpack({...})))
  end
end

local function eprintln(fmt, ...)
  eprint(fmt, unpack({...}))
  eprint("\n")
end

local function eflush()
  io.stderr:flush()
end

local function progress_cb()
  local seconds = os.time()
  return function()
    local now = os.time()
    if now - seconds > 1 then
      eprint(".")
      eflush()
      seconds = now
    end
    return 0
  end
end

local function exec(cmd, raw)
  eprintln(cmd)
  local f = assert(io.popen(cmd, 'r'))
  local s = assert(f:read('*a'))
  local code = f:close()
  if not code then
    eprintln(s)
    os.exit(false)
  end
  if raw then return s end
  s = string.gsub(s, '^%s+', '')
  s = string.gsub(s, '%s+$', '')
  s = string.gsub(s, '[\n\r]+', ' ')
  return s
end

-- Public API

local function save(content, filepath)
  eprintln("echo <%d bytes> > %s", #content, filepath)

  libc.mkdir(path.dirname(filepath))

  local file = assert(io.open(filepath, "w"))
  file:write(content)
  file:close()
end

local function copy(old_path, new_path, new_mode, subst)
  local old_file = assert(io.open(old_path))
  local new_file = assert(io.open(new_path, "w"))

  while true do
    local block = old_file:read(2^16)
    if not block then
      break
    end

    if subst then
      for k, v in pairs(subst) do
        block = string.gsub(block, k, v)
      end
    end
    new_file:write(block)
  end
  old_file:close()
  new_file:close()

  if new_mode then
    libc.chmod(new_file, new_mode)
  end
end

local function download(url, out)
  eprint("test -f %s: # $?: ", out)
  if libc.exists(out) then
    eprintln("EXISTS")
    return
  else
    eprintln("NOT_FOUND")
    libc.mkdir(path.dirname(out))
  end

  eprint("curl %s -o %s", url, out)
  eflush()

  local ch = libcurl.curl_easy_init()

  if ch then
    local file = io.open(out, "w")
    libcurl.curl_easy_setopt(ch, libcurl.CURLOPT_URL, url)
    libcurl.curl_easy_setopt(ch, libcurl.CURLOPT_FOLLOWLOCATION, true)
    libcurl.curl_easy_setopt(ch, libcurl.CURLOPT_WRITEDATA, file)

    libcurl.curl_easy_setopt(ch, libcurl.CURLOPT_NOPROGRESS, false)
    local cb_ptr = ffi.cast("curl_progress_callback", progress_cb())
    libcurl.curl_easy_setopt(ch, libcurl.CURLOPT_PROGRESSFUNCTION, cb_ptr)

    local result = libcurl.curl_easy_perform(ch)

    assert(
      result == libcurl.CURLE_OK,
      ffi.string(libcurl.curl_easy_strerror(result))
    )

    file:close()
    libcurl.curl_easy_cleanup(ch)
  end
  eprintln()
end

local function extract(file, path)
  eprint("bsdtar -xf %s -C %s", file, path)
  eflush()
  libc.mkdir(path)
  libarchive.extract(file, path, progress_cb())
  eprintln()
end

local function verify(algorithm, digest, file)
  eprintln("%ssum -c %s", algorithm, file)

  local f = assert(io.open(file, "rb"))
  local content = f:read("*all")
  f:close()
  local raw_digest = libcrypto.digest(algorithm, content)
  local file_digest = raw_digest:gsub(
    '.', function (c)
      return string.format('%02x', string.byte(c))
  end)
  assert(
    digest == file_digest,
    string.format("computed checksum did NOT match: %s != %s", digest, file_digest))
end

local function cd(path, block)
  local result = nil
  local cwd = libc.cwd()
  if not block then
    eprintln("cd " .. path)
  else
    eprintln("pushd " .. path)
  end
  libc.cd(path)
  if block then
    result = block()
    libc.cd(cwd)
    eprintln("popd # $PWD: %s", libc.cwd())
  end
  return result
end

local function install(mode, file, outdir, new_name)
  eprintln("install -m 0%o %s %s", mode, file, outdir)

  libc.mkdir(outdir)
  local name = new_name or path.basename(file)
  copy(file, path.join(outdir, name), mode)
end

local function sed(src, dst, subst)
  local patterns = {}
  for k, v in pairs(subst) do
    table.insert(patterns, string.format("-e 's/%s/%s/g'", k, v))
  end
  local pat = table.concat(patterns, " ")
  eprintln("sed %s %s > %s", pat, src, dst)

  copy(src, dst, nil, subst)
end

local function zig_build(cfg, options)
  if not cfg.args.dry_run then
    local zig = os.getenv("ZIG") or "zig"
    local cmd = ""
    if cfg.args.build then
      cmd = "build"
    elseif cfg.args.run then
      cmd = "build run"
    elseif cfg.args.test then
      cmd = "build test"
    end
    local verbosity = cfg.args.verbosity
    if verbosity and verbosity > 1 then
      options = options .. " --verbose-cc --verbose-link "
    end
    if cfg.args.ext then
      options = options .. " -- " .. table.concat(cfg.args.ext, " ")
    end
    local cmdline = string.format(
      "%s %s --build-file %s %s",
      zig, cmd, path.join(cfg.pkg.dir, "build.zig"), options)
    local out = exec(cmdline, true)
    if (cfg.args.run and not cfg.args.no_echo) or cfg.args.test then
      print(out)
    end
    return out
  end
end

local function touch(path)
  eprintln("touch " .. path)
  local new_file = assert(io.open(path, "a"))
  new_file:close()
end

local function ls(pattern)
  eprintln("ls " .. pattern)
  return libc.glob(pattern)
end

-- archive = {url, name, subdir, hash = {fn, digest}}
local function fetch(cfg, archive)
  local cache = path.join(path.tmpdir(), "slpm-cache")
  local dirname = table.concat(
    {
      cfg.args.target,
      cfg.args.mode,
      cfg.pkg.version
    }, "-")
  local result = path.join(cache, cfg.pkg.name, dirname)
  local archive_path = path.join(cache, cfg.pkg.name, archive.name)

  download(archive.url, archive_path)
  verify(archive.hash.fn, archive.hash.digest, archive_path)
  extract(archive_path, result)

  return path.join(result, archive.subdir)
end

return {
  private = {
    eprint = eprint,
    exec = exec,
    set_verbosity = set_verbosity,
  },
  public = {
    cd = cd,
    copy = copy,
    download = download,
    extract = extract,
    fetch = fetch,
    install = install,
    json_decode = json.decode,
    json_encode = json.encode,
    ls = ls,
    path = path,
    save = save,
    sed = sed,
    touch = touch,
    verify = verify,
    zig_build = zig_build,
  }
}
