local build

name = "luajit"
description = "LuaJIT scripting engine"
releases = "https://github.com/openresty/luajit2"

versions = {
  {
    version = "2.1-20210510",
    build = function(cfg, api)
      local sha256 = "1ee6dad809a5bb22efb45e6dac767f7ce544ad652d353a93d7f26b605f69fe3f"
      return build(cfg, api, sha256)
    end,
  }
}

build = function(cfg, api, archive_sha256)
  local luajit_version = "luajit2-" .. cfg.pkg.version
  local url_root = "https://github.com/openresty/luajit2/archive/refs/tags/v"

  local origin
  if not cfg.args.dry_run then
    origin = api.fetch(
      cfg, {
        url = url_root .. cfg.pkg.version .. ".tar.gz",
        name = luajit_version .. ".tar.gz",
        subdir = luajit_version,
        hash = {
          fn = "sha256",
          digest = archive_sha256
        }
    })

    local includes = {
      "lua.h", "lualib.h", "lauxlib.h", "luaconf.h", "lua.hpp", "luajit.h"
    }
    for _, i in ipairs(includes) do
      api.install(
        tonumber("0644", 8),
        api.path.join(origin, "src", i),
        cfg.prefix.includedir)
    end
  end

  local lib_name = string.format(
    "%s-%s-%s", cfg.pkg.name, cfg.args.mode, cfg.pkg.version)
  local options = {
    lib = {
      ["native/luajit"] = {
        name = lib_name,
        object = api.get_lib_object(cfg, lib_name),
        version = cfg.pkg.version,
      }
    },
    prefix = cfg.prefix,
    -- entries below are not required for dependencies
    mode = cfg.args.mode,
    origin = origin,
    target = cfg.args.target,
  }

  api.zig_build(cfg, string.format("-Doptions=%q", api.json_encode(options)))

  return {
    lib = options.lib,
    prefix = options.prefix,
  }
end
