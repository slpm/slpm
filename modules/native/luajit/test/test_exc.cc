#include <lua.hpp>
#include <stdexcept>

extern "C" int zig_div(lua_State* L) {
  const auto a = luaL_checknumber(L, 1);
  const auto b = luaL_checknumber(L, 2);

  if (b == 0) {
    throw std::logic_error("division by zero");
  }

  const auto c = a / b;

  lua_pushnumber(L, c);
  return 1;
}

extern "C" const char* ffi_lua = R""(
  -- tag::ffi_lua[]
  local ffi = require("ffi")
  ffi.cdef[[
  struct Stdlib {
    int (*abs)(int j);
    int (*atoi)(const char *nptr);
  };
  ]]
  local stdlib = ffi.cast("struct Stdlib*", stdlib_glue)
  assert(stdlib.atoi("123") == 123)
  assert(stdlib.abs(-1) == 1)
  -- end::ffi_lua[]
  )"";
