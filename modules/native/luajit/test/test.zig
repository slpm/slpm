const std = @import("std");
// tag::c-import[]
const c = @cImport({
    @cInclude("lua.h");
    @cInclude("lualib.h");
    @cInclude("lauxlib.h");
    @cInclude("stdlib.h");
});
// end::c-import[]

extern fn zig_div(L: ?*c.lua_State) c_int;
extern const ffi_lua: [*c]const u8;

export fn add(L: ?*c.lua_State) c_int {
    const a = c.luaL_checkinteger(L, 1);
    const b = c.luaL_checkinteger(L, 2);

    const res = a + b;

    c.lua_pushinteger(L, res);
    return 1;
}

test "add" {
    var L = c.luaL_newstate();
    defer c.lua_close(L);

    c.luaL_openlibs(L);

    c.lua_pushcfunction(L, add);
    c.lua_setglobal(L, "zig_add");

    try std.testing.expectEqual(
        c.luaL_loadstring(L, "return zig_add(3, 5)"),
        c.LUA_OK,
    );

    try std.testing.expectEqual(
        c.lua_pcall(L, 0, c.LUA_MULTRET, 0),
        c.LUA_OK,
    );

    const ret = c.luaL_checkinteger(L, 1);
    try std.testing.expectEqual(ret, 8);
}

test "bit ops" {
    var L = c.luaL_newstate();
    defer c.lua_close(L);

    c.luaL_openlibs(L);

    try std.testing.expectEqual(
        c.luaL_loadstring(L, "return bit.lshift(1, 3)"),
        c.LUA_OK,
    );

    try std.testing.expectEqual(
        c.lua_pcall(L, 0, c.LUA_MULTRET, 0),
        c.LUA_OK,
    );

    const ret = c.luaL_checkinteger(L, 1);
    try std.testing.expectEqual(ret, 8);
}

test "unwind exceptions" {
    var L = c.luaL_newstate();
    defer c.lua_close(L);

    c.luaL_openlibs(L);

    c.lua_pushcfunction(L, zig_div);
    c.lua_setglobal(L, "zig_div");

    try std.testing.expectEqual(
        c.luaL_loadstring(L, "return zig_div(6, 0)"),
        c.LUA_OK,
    );

    try std.testing.expectEqual(
        c.lua_pcall(L, 0, c.LUA_MULTRET, 0),
        c.LUA_ERRRUN,
    );

    const err = c.lua_tolstring(L, -1, null);

    try std.testing.expectEqualStrings("C++ exception", std.mem.span(err));
}

test "ffi" {
    var L = c.luaL_newstate();
    defer c.lua_close(L);
    c.luaL_openlibs(L);

    // tag::export[]
    const Stdlib = extern struct {
        abs: fn (j: c_int) callconv(.C) c_int,
        atoi: fn (nptr: [*c]const u8) callconv(.C) c_int,
    };
    var stdlib = Stdlib{
        .abs = c.abs,
        .atoi = c.atoi,
    };
    c.lua_pushlightuserdata(L, &stdlib);
    c.lua_setglobal(L, "stdlib_glue");
    // end::export[]

    try std.testing.expectEqual(c.luaL_loadstring(L, ffi_lua), c.LUA_OK);

    try std.testing.expectEqual(
        c.lua_pcall(L, 0, c.LUA_MULTRET, 0),
        c.LUA_OK,
    );
}
