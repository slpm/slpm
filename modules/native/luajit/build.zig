const ArrayList = std.ArrayList;
const ChildProcess = std.ChildProcess;
const Step = std.build.Step;

const ExecutorRunStep = struct {
    const Self = @This();

    pub const base_id = .custom;

    step: Step,
    builder: *Builder,
    artifact: *LibExeObjStep,
    argv: ArrayList(Str),

    pub fn init(b: *Builder, artifact: *LibExeObjStep) Self {
        var step = Step.init(
            .custom,
            b.fmt("{s} {s}", .{ @typeName(Self), artifact.name }),
            b.allocator,
            make,
        );
        step.dependOn(&artifact.step);
        return Self{
            .builder = b,
            .step = step,
            .artifact = artifact,
            .argv = ArrayList(Str).init(b.allocator),
        };
    }

    pub fn addArg(self: *Self, arg: []const u8) void {
        self.argv.append(self.builder.dupe(arg)) catch unreachable;
    }

    fn getExternalExecutor(artifact: *LibExeObjStep) ?[]const u8 {
        switch (artifact.target.getExternalExecutor()) {
            .native, .unavailable => {
                return null;
            },
            .qemu => |bin_name| {
                return bin_name;
            },
            .wine => |bin_name| {
                return bin_name;
            },
            .wasmtime => |bin_name| {
                return bin_name;
            },
            .darling => |bin_name| {
                return bin_name;
            },
        }
    }

    fn make(step: *Step) !void {
        const self = @fieldParentPtr(Self, "step", step);
        const exe = self.artifact.installed_path orelse
            self.artifact.getOutputSource().getPath(self.builder);
        var cmd = ArrayList(Str).init(self.builder.allocator);
        if (getExternalExecutor(self.artifact)) |executor| {
            try cmd.append(executor);
        }
        try cmd.append(exe);
        for (self.argv.items) |arg| {
            try cmd.append(arg);
        }
        _ = try ChildProcess.exec(
            .{ .allocator = self.builder.allocator, .argv = cmd.items },
        );
    }
};

fn executorRun(b: *Builder, artifact: *LibExeObjStep) !*ExecutorRunStep {
    const executorRunStep = try b.allocator.create(ExecutorRunStep);
    executorRunStep.* = ExecutorRunStep.init(b, artifact);
    return executorRunStep;
}

fn genericName(arch: Target.Cpu.Arch) Str {
    return switch (arch) {
        .arm, .armeb, .thumb, .thumbeb => "arm",
        .aarch64, .aarch64_be, .aarch64_32 => "arm64",
        .mips, .mipsel => "mips",
        .mips64, .mips64el => "mips64",
        .powerpc, .powerpcle, .powerpc64, .powerpc64le => "ppc",
        .s390x => "s390x",
        .i386 => "x86",
        .x86_64 => "x64",
        else => @tagName(arch),
    };
}

fn ver(arch: Target.Cpu.Arch, features: Target.Cpu.Feature.Set) Str {
    return switch (arch) {
        .arm, .armeb, .thumb, .thumbeb => blck: {
            const has = Target.arm.featureSetHasAny;
            if (has(features, .{ .v7a, .v7em, .v7k, .v7m, .v7r, .v7s, .v7ve })) {
                break :blck "70";
            } else if (has(features, .{ .v8_1a, .v8_1m_main, .v8_2a, .v8_3a, .v8_4a, .v8_5a, .v8_6a, .v8_7a, .v8a, .v8m, .v8m_main, .v8r })) {
                break :blck "80";
            } else if (has(features, .{ .v6, .v6j, .v6k, .v6kz, .v6m, .v6sm, .v6t2 })) {
                break :blck "60";
            } else if (has(features, .{.v6t2})) {
                break :blck "61";
            } else {
                break :blck "50";
            }
        },
        .aarch64, .aarch64_be, .aarch64_32 => "80",
        // More granular approach could be used only if PPC CPU specified
        .powerpc, .powerpcle, .powerpc64, .powerpc64le => "0",
        .mips, .mipsel, .mips64, .mips64el => blck: {
            const has = Target.mips.featureSetHasAny;
            if (has(features, .{ .mips32r6, .mips64r6 })) {
                break :blck "60";
            } else if (has(features, .{ .mips32r2, .mips64r2 })) {
                break :blck "20";
            } else {
                break :blck "10";
            }
        },
        else => "",
    };
}

fn hasFpu(arch: Target.Cpu.Arch, features: Target.Cpu.Feature.Set) bool {
    return switch (arch) {
        .arm, .armeb, .thumb, .thumbeb => !Target.arm.featureSetHas(features, .soft_float),
        .mips, .mipsel, .mips64, .mips64el => !Target.mips.featureSetHas(features, .soft_float),
        .powerpc, .powerpcle, .powerpc64, .powerpc64le => Target.powerpc.featureSetHas(features, .hard_float),
        else => true,
    };
}

pub fn build(b: *Builder) !void {
    const opts = try getOptions(b);

    const c_flags = [_][]const u8{"-std=gnu99"};
    const c_files = [_]Str{
        "src/lib_aux.c",
        "src/lib_base.c",
        "src/lib_bit.c",
        "src/lib_debug.c",
        "src/lib_ffi.c",
        "src/lib_init.c",
        "src/lib_io.c",
        "src/lib_jit.c",
        "src/lib_math.c",
        "src/lib_os.c",
        "src/lib_package.c",
        "src/lib_string.c",
        "src/lib_table.c",
        "src/lj_alloc.c",
        "src/lj_api.c",
        "src/lj_asm.c",
        "src/lj_bc.c",
        "src/lj_bcread.c",
        "src/lj_bcwrite.c",
        "src/lj_buf.c",
        "src/lj_carith.c",
        "src/lj_ccall.c",
        "src/lj_ccallback.c",
        "src/lj_cconv.c",
        "src/lj_cdata.c",
        "src/lj_char.c",
        "src/lj_clib.c",
        "src/lj_cparse.c",
        "src/lj_crecord.c",
        "src/lj_ctype.c",
        "src/lj_debug.c",
        "src/lj_dispatch.c",
        "src/lj_err.c",
        "src/lj_ffrecord.c",
        "src/lj_func.c",
        "src/lj_gc.c",
        "src/lj_gdbjit.c",
        "src/lj_ir.c",
        "src/lj_lex.c",
        "src/lj_lib.c",
        "src/lj_load.c",
        "src/lj_mcode.c",
        "src/lj_meta.c",
        "src/lj_obj.c",
        "src/lj_opt_dce.c",
        "src/lj_opt_fold.c",
        "src/lj_opt_loop.c",
        "src/lj_opt_mem.c",
        "src/lj_opt_narrow.c",
        "src/lj_opt_sink.c",
        "src/lj_opt_split.c",
        "src/lj_parse.c",
        "src/lj_prng.c",
        "src/lj_profile.c",
        "src/lj_record.c",
        "src/lj_snap.c",
        "src/lj_state.c",
        "src/lj_str.c",
        "src/lj_strfmt.c",
        "src/lj_strfmt_num.c",
        "src/lj_strscan.c",
        "src/lj_tab.c",
        "src/lj_trace.c",
        "src/lj_udata.c",
        "src/lj_vmevent.c",
        "src/lj_vmmath.c",
    };

    const minilua_files = [_]Str{
        "src/host/minilua.c",
    };

    const buildvm_files = [_]Str{
        "src/host/buildvm.c",
        "src/host/buildvm_asm.c",
        "src/host/buildvm_peobj.c",
        "src/host/buildvm_lib.c",
        "src/host/buildvm_fold.c",
    };

    const lib_files = [_]Str{
        "src/lib_base.c",
        "src/lib_math.c",
        "src/lib_bit.c",
        "src/lib_string.c",
        "src/lib_table.c",
        "src/lib_io.c",
        "src/lib_os.c",
        "src/lib_package.c",
        "src/lib_debug.c",
        "src/lib_jit.c",
        "src/lib_ffi.c",
    };

    const hdr_files = [_]Str{
        "bcdef", "ffdef", "libdef", "recdef",
    };

    const minilua = b.addExecutable("minilua", null);
    minilua.linkLibC();
    // minilua doesn't pass sanitizers enabled in default debug mode
    minilua.setBuildMode(Mode.ReleaseSmall);
    addCSourceFiles(b, minilua, minilua_files[0..], c_flags[0..], opts);

    const run_dynasm = minilua.run();
    run_dynasm.addArg(normalized(b, opts.origin, "dynasm/dynasm.lua"));
    var flags = ArrayList(Str).init(b.allocator);
    defer flags.deinit();

    // Enable all relavant flags
    try flags.append(switch (opts.cpu_arch.endian()) {
        .Big => "ENDIAN_BE",
        .Little => "ENDIAN_LE",
    });
    try flags.append("JIT"); // TODO: Skip for PowerPC SPE, iOS and consoles
    try flags.append("FFI"); // TODO: Skip for PowerPC and consoles
    const ver_str = b.fmt("VER={s}", .{ver(opts.cpu_arch, opts.cpu_features)});
    try flags.append(ver_str);
    const has_fpu = hasFpu(opts.cpu_arch, opts.cpu_features);
    if (has_fpu) {
        try flags.append("FPU");
    } else {
        try flags.append("HFABI");
    }
    if (opts.os_tag == .windows) {
        try flags.append("WIN");
    }
    // try flags.append("DUALNUM"); // TODO: Apply proper detection
    //try flags.append("NO_UNWIND"); // TODO: Skip if libunwind not available
    // Skipping "-D SSE"
    // Skipping "-D IOS"
    // Skipping "-D SQRT"
    // Skipping "-D ROUND"
    // Skipping "-D GPR64"
    // Skipping "-D PPE -D TOC"

    try flags.append(b.fmt("P{}", .{opts.cpu_bits}));

    var args = ArrayList(Str).init(b.allocator);
    defer args.deinit();

    for (flags.items) |elem| {
        try args.append("-D");
        try args.append(elem);
    }

    try args.append("-o");
    try args.append(normalized(b, opts.origin, "src/host/buildvm_arch.h"));

    const buildvm = b.addExecutable("buildvm", null);
    const arch_name = genericName(opts.cpu_arch);

    buildvm.defineCMacro("LUAJIT_TARGET", b.fmt("LUAJIT_ARCH_{s}", .{arch_name}));
    buildvm.defineCMacro("LJ_ARCH_HASFPU", b.fmt("{}", .{@boolToInt(has_fpu)}));
    buildvm.defineCMacro("LJ_ABI_SOFTFP", b.fmt("{}", .{@boolToInt(!has_fpu)}));
    const dasc = b.fmt("src/vm_{s}.dasc", .{arch_name});
    try args.append(normalized(b, opts.origin, dasc));

    for (args.items) |elem| {
        run_dynasm.addArg(elem);
    }
    run_dynasm.step.dependOn(&minilua.step);

    buildvm.step.dependOn(&run_dynasm.step);
    buildvm.addIncludeDir(normalized(b, opts.origin, "src"));
    buildvm.linkLibC();
    buildvm.setTarget(opts.target);
    // buildvm doesn't pass sanitizers enabled in default debug mode
    buildvm.setBuildMode(Mode.ReleaseSmall);
    addCSourceFiles(b, buildvm, buildvm_files[0..], c_flags[0..], opts);

    const lib = b.addStaticLibrary(getLibName("native/luajit", opts), null);
    addCSourceFiles(b, lib, c_files[0..], c_flags[0..], opts);
    setBuildMode(lib, opts);
    setLibOutputDir(lib, opts);

    lib.step.dependOn(&buildvm.step);
    lib.linkLibC();
    lib.setTarget(opts.target);
    lib.defineCMacro("LJ_ABI_SOFTFP", b.fmt("{}", .{@boolToInt(!has_fpu)}));

    if (opts.os_tag == .windows) {
        lib.addObjectFile(normalized(b, opts.origin, "src/lj_vm.o"));
    } else {
        lib.addAssemblyFile(normalized(b, opts.origin, "src/lj_vm.s"));
    }
    lib.install();

    for (hdr_files) |hdr| {
        const lj_def = try executorRun(b, buildvm);
        lj_def.addArg("-m");
        lj_def.addArg(hdr);
        lj_def.addArg("-o");
        lj_def.addArg(normalized(b, opts.origin, b.fmt("src/lj_{s}.h", .{hdr})));
        for (lib_files) |lib_file| {
            lj_def.addArg(normalized(b, opts.origin, lib_file));
        }
        lib.step.dependOn(&lj_def.step);
    }

    const lj_folddef = try executorRun(b, buildvm);
    lj_folddef.addArg("-m");
    lj_folddef.addArg("folddef");
    lj_folddef.addArg("-o");
    lj_folddef.addArg(normalized(b, opts.origin, "src/lj_folddef.h"));
    lj_folddef.addArg(normalized(b, opts.origin, "src/lj_opt_fold.c"));
    lib.step.dependOn(&lj_folddef.step);

    const lj_vm = try executorRun(b, buildvm);
    lj_vm.addArg("-m");
    lj_vm.addArg(switch (opts.os_tag) {
        .windows => "peobj",
        .macos => "machasm",
        else => "elfasm",
    });
    lj_vm.addArg("-o");
    lj_vm.addArg(normalized(
        b,
        opts.origin,
        if (opts.os_tag == .windows) "src/lj_vm.o" else "src/lj_vm.s",
    ));
    lib.step.dependOn(&lj_vm.step);

    b.default_step.dependOn(&lib.step);

    const test_exe = b.addTest("test/test.zig");
    addObjectFiles(test_exe, opts);
    addIncludeDirs(test_exe, opts);

    const cc_flags = [_]Str{};
    test_exe.addCSourceFile("test/test_exc.cc", cc_flags[0..]);
    test_exe.linkLibC();

    test_exe.linkLibCpp();
    // https://github.com/ziglang/zig/issues/6573
    test_exe.single_threaded = true;

    test_exe.linkSystemLibrary("unwind");
    test_exe.setTarget(opts.target);
    test_exe.enable_qemu = true;
    test_exe.enable_wine = true;
    test_exe.step.dependOn(&lib.step);

    const test_step = b.step("test", "Test the library");
    test_step.dependOn(&test_exe.step);
}

// BEGIN GENERATED CONTENT
const std = @import("std");

const Builder = std.build.Builder;
const CrossTarget = std.zig.CrossTarget;
const LibExeObjStep = std.build.LibExeObjStep;
const Mode = std.builtin.Mode;
const ObjectMap = std.json.ObjectMap;
const Str = []const u8;
const Target = std.Target;

const Options = struct {
    bindir: ?Str,
    cpu_arch: Target.Cpu.Arch,
    cpu_bits: usize,
    cpu_features: Target.Cpu.Feature.Set,
    libdir: ?Str,
    mode: Mode,
    obj: ObjectMap,
    origin: ?Str,
    os_tag: Target.Os.Tag,
    target: CrossTarget,
};

fn addCSourceFiles(
    b: *Builder,
    artifact: *LibExeObjStep,
    files: []const Str,
    flags: []const Str,
    opts: Options,
) void {
    for (files) |c_file| {
        artifact.addCSourceFile(normalized(b, opts.origin, c_file), flags);
    }
}

fn addIncludeDirs(artifact: *LibExeObjStep, opts: Options) void {
    const add = (struct {
        artifact: *LibExeObjStep,
        fn add(self: @This(), obj: ObjectMap) void {
            if (obj.get("includedir")) |dir| {
                self.artifact.addIncludeDir(dir.String);
            }
        }
    }{ .artifact = artifact }).add;

    if (opts.obj.get("dep_opts")) |dep_opts| {
        if (dep_opts.Object.get("prefix")) |prefix| {
            for (prefix.Array.items) |v| {
                add(v.Object);
            }
        }
    }

    if (opts.obj.get("prefix")) |prefix| {
        add(prefix.Object);
    }
}

fn addObjectFiles(artifact: *LibExeObjStep, opts: Options) void {
    const add = (struct {
        artifact: *LibExeObjStep,
        fn add(self: @This(), obj: ObjectMap) void {
            var it = obj.iterator();
            while (it.next()) |raw_entry| {
                const entry = raw_entry.value_ptr.*;
                if (entry.Object.get("object")) |object| {
                    self.artifact.addObjectFile(object.String);
                }
            }
        }
    }{ .artifact = artifact }).add;

    if (opts.obj.get("dep_opts")) |dep_opts| {
        if (dep_opts.Object.get("lib")) |lib| {
            for (lib.Array.items) |v| {
                add(v.Object);
            }
        }
    }

    if (opts.obj.get("lib")) |lib| {
        add(lib.Object);
    }
}

fn addPackagePaths(artifact: *LibExeObjStep, opts: Options) void {
    const add = (struct {
        artifact: *LibExeObjStep,
        fn add(self: @This(), obj: ObjectMap) void {
            var it = obj.iterator();
            while (it.next()) |raw_entry| {
                const key = raw_entry.key_ptr.*;
                const entry = raw_entry.value_ptr.*;
                if (entry.Object.get("zigpkg")) |object| {
                    self.artifact.addPackagePath(key, object.String);
                }
            }
        }
    }{ .artifact = artifact }).add;

    if (opts.obj.get("dep_opts")) |dep_opts| {
        if (dep_opts.Object.get("lib")) |lib| {
            for (lib.Array.items) |v| {
                add(v.Object);
            }
        }
    }

    if (opts.obj.get("lib")) |lib| {
        add(lib.Object);
    }
}

fn getBinName(selector: Str, opts: Options) Str {
    if (opts.obj.get("bin")) |binobj| {
        if (binobj.Object.get(selector)) |main| {
            if (main.Object.get("name")) |v| {
                return v.String;
            }
        }
    }
    return "bin";
}

fn getLibName(selector: Str, opts: Options) Str {
    if (opts.obj.get("lib")) |libobj| {
        if (libobj.Object.get(selector)) |main| {
            if (main.Object.get("name")) |v| {
                return v.String;
            }
        }
    }
    return "lib";
}

fn getOptions(b: *Builder) !Options {
    var target = b.standardTargetOptions(.{});
    var mode = b.standardReleaseOptions();

    var opt_origin: ?Str = null;
    var opt_libdir: ?Str = null;
    var opt_bindir: ?Str = null;

    var options_obj = std.json.ObjectMap.init(b.allocator);
    const opt_options = b.option(
        Str,
        "options",
        "Project options in JSON format",
    );
    if (opt_options) |text| {
        var p = std.json.Parser.init(b.allocator, false);
        defer p.deinit();

        options_obj = (try p.parse(text)).root.Object;
        if (options_obj.get("origin")) |origin| {
            opt_origin = origin.String;
        }
        if (options_obj.get("prefix")) |v| {
            if (v.Object.get("libdir")) |libdir| {
                opt_libdir = libdir.String;
            }
            if (v.Object.get("bindir")) |bindir| {
                opt_bindir = bindir.String;
            }
        }
    }

    if (options_obj.get("target")) |v| {
        target = try CrossTarget.parse(.{ .arch_os_abi = v.String });
    }

    if (options_obj.get("mode")) |v| {
        mode = if (std.mem.eql(u8, v.String, "safe"))
            Mode.ReleaseSafe
        else if (std.mem.eql(u8, v.String, "fast"))
            Mode.ReleaseFast
        else if (std.mem.eql(u8, v.String, "small"))
            Mode.ReleaseSmall
        else
            Mode.Debug;
    }

    const target_info = try std.zig.system.NativeTargetInfo.detect(
        b.allocator,
        target,
    );
    const resolved_target = target_info.target;
    const arch = resolved_target.cpu.arch;
    const features = resolved_target.cpu.features;
    const os_tag = resolved_target.os.tag;

    return Options{
        .bindir = opt_bindir,
        .cpu_arch = arch,
        .cpu_bits = arch.ptrBitWidth(),
        .cpu_features = features,
        .libdir = opt_libdir,
        .mode = mode,
        .obj = options_obj,
        .origin = opt_origin,
        .os_tag = os_tag,
        .target = target,
    };
}

fn normalized(b: *Builder, opt_prefix: ?Str, path: Str) Str {
    if (opt_prefix) |prefix| {
        return b.fmt("{s}/{s}", .{ prefix, path });
    } else {
        return path;
    }
}

fn setBuildMode(artifact: *LibExeObjStep, opts: Options) void {
    artifact.setBuildMode(opts.mode);
    if (opts.mode == .ReleaseSmall) {
        artifact.strip = true;
    }
}

fn setLibOutputDir(artifact: *LibExeObjStep, opts: Options) void {
    if (opts.libdir) |libdir| {
        artifact.setOutputDir(libdir);
    }
}

fn setBinOutputDir(artifact: *LibExeObjStep, opts: Options) void {
    if (opts.bindir) |bindir| {
        artifact.setOutputDir(bindir);
    }
}
// END GENERATED CONTENT
