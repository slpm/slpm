const std = @import("std");
const lua = @cImport({
    @cInclude("lua.h");
    @cInclude("lualib.h");
    @cInclude("lauxlib.h");
});

export fn add(s: ?*lua.lua_State) c_int {
    const a = lua.luaL_checkinteger(s, 1);
    const b = lua.luaL_checkinteger(s, 2);

    const c = a + b;

    lua.lua_pushinteger(s, c);
    return 1;
}

test "add" {
    var s = lua.luaL_newstate();
    defer lua.lua_close(s);

    lua.luaL_openlibs(s);

    lua.lua_pushcfunction(s, add);
    lua.lua_setglobal(s, "zig_add");

    try std.testing.expectEqual(
        lua.luaL_loadstring(s, "return zig_add(3, 5)"),
        lua.LUA_OK,
    );

    try std.testing.expectEqual(
        lua.lua_pcallk(s, 0, lua.LUA_MULTRET, 0, 0, null),
        lua.LUA_OK,
    );

    const ret = lua.luaL_checkinteger(s, 1);
    try std.testing.expectEqual(ret, 8);
}
