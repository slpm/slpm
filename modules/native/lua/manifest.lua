local build

name = "lua"
description = "Lua scripting engine"
releases = "https://www.lua.org/download.html"

versions = {
  {
    version = "5.4.3",
    build = function(cfg, api)
      local sha1 = "1dda2ef23a9828492b4595c0197766de6e784bc7"
      return build(cfg, api, sha1)
    end,
  }
}

build = function(cfg, api, archive_sha1)
  local lua_version = "lua-" .. cfg.pkg.version
  local url_root = "https://www.lua.org/ftp/"

  local origin
  if not cfg.args.dry_run then
    origin = api.fetch(
      cfg, {
        url = url_root .. lua_version .. ".tar.gz",
        name = lua_version .. ".tar.gz",
        subdir = lua_version,
        hash = {
          fn = "sha1",
          digest = archive_sha1
        }
    })

    local includes = {
      "lua.h", "luaconf.h", "lualib.h", "lauxlib.h", "lua.hpp"
    }
    for _, i in ipairs(includes) do
      api.install(
        tonumber("0644", 8),
        api.path.join(origin, "src", i),
        cfg.prefix.includedir)
    end
  end

  local lib_name = string.format(
    "%s-%s-%s", cfg.pkg.name, cfg.args.mode, cfg.pkg.version)
  local options = {
    lib = {
      ["native/lua"] = {
        name = lib_name,
        object = api.get_lib_object(cfg, lib_name),
        version = cfg.pkg.version,
      }
    },
    prefix = cfg.prefix,
    -- entries below are not required for dependencies
    mode = cfg.args.mode,
    origin = origin,
    target = cfg.args.target,
  }

  api.zig_build(cfg, string.format("-Doptions=%q", api.json_encode(options)))

  return {
    lib = options.lib,
    prefix = options.prefix,
  }
end
