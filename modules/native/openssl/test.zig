const std = @import("std");
const c = @cImport({
    @cInclude("openssl/engine.h");
    @cInclude("openssl/evp.h");
    @cInclude("openssl/rsa.h");
});
const assert = std.debug.assert;

fn print_cb(str: [*c]const u8, len: usize, _: ?*c_void) callconv(.C) c_int {
    std.debug.print("{s}\n", .{str[0..len]});
    return 0;
}

fn ssl_assert(cond: bool) void {
    if (!cond) {
        std.debug.print("\n", .{});
        c.ERR_print_errors_cb(print_cb, null);
        unreachable;
    }
}

test "add" {
    const md = c.EVP_get_digestbyname("sha1");
    ssl_assert(md != null);

    const ctx = c.EVP_MD_CTX_new();
    ssl_assert(ctx != null);
    defer c.EVP_MD_CTX_free(ctx);

    ssl_assert(c.EVP_DigestInit_ex(ctx, md, null) != 0);

    const str = "test";
    ssl_assert(c.EVP_DigestUpdate(ctx, str, str.len) != 0);

    var buf: [c.EVP_MAX_MD_SIZE]u8 = undefined;
    var len: [1]c_uint = undefined;

    ssl_assert(c.EVP_DigestFinal_ex(ctx, &buf, &len) != 0);

    const result = buf[0..len[0]];
    var p: [2 * c.EVP_MAX_MD_SIZE]u8 = undefined;
    try std.testing.expectEqualStrings(
        "a94a8fe5ccb19ba61c4c0873d391e987982fbbd3",
        try std.fmt.bufPrint(&p, "{}", .{std.fmt.fmtSliceHexLower(result)}),
    );
}

fn p11_public_encrypt(
    flen: c_int,
    from: [*c]const u8,
    to: [*c]u8,
    rsa: ?*c.RSA,
    padding: c_int,
) callconv(.C) c_int {
    assert(flen == 4);
    assert(from != null);
    assert(to != null);
    assert(rsa != null);
    assert(padding == c.RSA_PKCS1_OAEP_PADDING);
    @memset(to, 'a', 256);
    return 256;
}

fn loadkeys(
    engine: ?*c.ENGINE,
    key_id: [*c]const u8,
    ui_method: ?*c.UI_METHOD,
    callback_data: ?*c_void,
) callconv(.C) ?*c.EVP_PKEY {
    assert(engine != null);
    assert(key_id != null);
    assert(ui_method == null);
    assert(callback_data == null);
    return c.EVP_PKEY_new();
}

test "engine" {
    // ==== Engine registration section ====

    // Create a new engine
    const engine = c.ENGINE_new();
    ssl_assert(engine != null);
    defer _ = c.ENGINE_free(engine);

    ssl_assert(c.ENGINE_set_id(engine, "pkcs11") != 0);
    const engine_name = "pkcs11 test engine";
    ssl_assert(c.ENGINE_set_name(engine, engine_name) != 0);

    // Register RSA functions within the engine

    // Please note that it depends on OpenSSL version.
    // Version 1.1.x has opaque RSA_METHOD (rsa_meth_st) type,
    // thus RSA_meth_* functions should be used,
    // direct initialization of the struct won't work.
    const p11_rsa = c.RSA_meth_new("pkcs11 PKCS#1 RSA", 0);
    defer c.RSA_meth_free(p11_rsa);

    ssl_assert(c.RSA_meth_set_pub_enc(p11_rsa, p11_public_encrypt) != 0);
    ssl_assert(c.ENGINE_set_RSA(engine, p11_rsa) != 0);

    // Register a function to load public keys
    ssl_assert(c.ENGINE_set_load_pubkey_function(engine, loadkeys) != 0);

    // Register the engine
    ssl_assert(c.ENGINE_add(engine) != 0);

    // ==== Engine use section ====

    // Fetch the engine
    const p11_engine = c.ENGINE_by_id("pkcs11");
    try std.testing.expectEqualStrings(
        std.mem.span(c.ENGINE_get_name(p11_engine)),
        engine_name,
    );

    // The references are the same, so no ENGINE_free here,
    // but the userspace code should free the engine
    try std.testing.expectEqual(p11_engine, engine);

    // Initialize the engine (required for the refcounts)
    ssl_assert(c.ENGINE_init(p11_engine) != 0);
    defer _ = c.ENGINE_finish(p11_engine);

    // Test public key loading (key is refcounted)
    const key = c.ENGINE_load_public_key(p11_engine, "key_id", null, null);
    ssl_assert(key != null);

    // Use RSA functions from the engine
    ssl_assert(c.ENGINE_set_default_RSA(p11_engine) != 0);

    // Generate RSA key
    const exponent = c.BN_new();
    ssl_assert(exponent != null);
    defer c.BN_clear_free(exponent);

    ssl_assert(c.BN_set_word(exponent, c.RSA_F4) != 0);

    const rsa = c.RSA_new();
    ssl_assert(rsa != null);
    ssl_assert(c.RSA_generate_key_ex(rsa, 2048, exponent, null) != 0);

    // Test RSA encryption
    const buf = "test";
    const outlen = @intCast(usize, c.RSA_size(rsa));
    const out = c.OPENSSL_malloc(outlen);
    ssl_assert(out != null);
    defer c.OPENSSL_clear_free(out, outlen);

    try std.testing.expectEqual(@intCast(usize, c.RSA_public_encrypt(
        buf.len,
        buf,
        @ptrCast([*c]u8, out),
        rsa,
        c.RSA_PKCS1_OAEP_PADDING,
    )), outlen);

    const expected = "a" ** 256;
    try std.testing.expectEqualStrings(
        std.mem.span(@ptrCast([*c]const u8, out)),
        expected[0..],
    );
}
