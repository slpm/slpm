local build

name = "openssl"
description = "Cryptography and SSL/TLS toolkit"
releases = "https://www.openssl.org/source/"

versions = {
  {
    version = "1.1.1l",
    build = function(cfg, api)
      local sha1 = "f8819dd31642eebea6cc1fa5c256fc9a4f40809b"
      return build(cfg, api, sha1)
    end,
  }
}

local generated_headers

build = function(cfg, api, archive_sha1)
  local openssl_version = "openssl-" .. cfg.pkg.version
  local url_root = "https://www.openssl.org/source/"

  local origin
  if not cfg.args.dry_run then
    origin = api.fetch(
      cfg, {
        url = url_root .. openssl_version .. ".tar.gz",
        name = openssl_version .. ".tar.gz",
        subdir = openssl_version,
        hash = {
          fn = "sha1",
          digest = archive_sha1
        }
    })

    local origin_includes = api.path.join(origin, "include")
    local info = api.get_target_info(cfg.args.target)
    local headers = generated_headers(info)
    for k, v in pairs(headers) do
      api.save(v, api.path.join(origin, k))
    end
    -- OpenSSL make process uses *.h glob for installing headers
    local pattern = api.path.join(origin_includes, "openssl", "*.h")
    local includes = api.ls(pattern)
    for _, i in ipairs(includes) do
      api.install(
        tonumber("0644", 8),
        i,
        api.path.join(cfg.prefix.includedir, "openssl"))
    end
  end

  local lib_crypto_name = string.format(
    "crypto-%s-%s", cfg.args.mode, cfg.pkg.version)
  local lib_ssl_name = string.format(
    "ssl-%s-%s", cfg.args.mode, cfg.pkg.version)
  local options = {
    lib = {
      ["native/crypto"] = {
        name = lib_crypto_name,
        object = api.get_lib_object(cfg, lib_crypto_name),
        version = cfg.pkg.version,
      },
      -- ["native/ssl"] = {
      --   name = lib_ssl_name,
      --   object = api.get_lib_object(cfg, lib_ssl_name),
      --   version = cfg.pkg.version,
      -- },
    },
    prefix = cfg.prefix,
    -- entries below are not required for dependencies
    mode = cfg.args.mode,
    origin = origin,
    target = cfg.args.target,
  }

  api.zig_build(cfg, string.format("-Doptions=%q", api.json_encode(options)))

  return {
    lib = options.lib,
    prefix = options.prefix,
  }
end

local function get_bits(target_info)
  local all = {
    "SIXTY_FOUR_BIT_LONG",
    "SIXTY_FOUR_BIT",
    "THIRTY_TWO_BIT"
  }
  -- LP64: SIXTY_FOUR_BIT_LONG
  -- LLP64: SIXTY_FOUR_BIT
  local value = "SIXTY_FOUR_BIT_LONG"
  if target_info.bits == 32 then
    value = "THIRTY_TWO_BIT"
  elseif target_info.os == "windows" then
    value = "SIXTY_FOUR_BIT"
  end
  local lines = {"/* Only one for the following should be defined */"}
  for _, v in ipairs(all) do
    local directive = "undef"
    if v == value then
      directive = "define"
    end
    table.insert(lines, string.format("# %s %s", directive, v))
  end
  return table.concat(lines, "\n") .. "\n"
end

generated_headers = function(target_info)
  local bits = get_bits(target_info)
  local dso = [[
# define DSO_DLFCN
# define HAVE_DLFCN_H
# define DSO_EXTENSION ".so"
]]
  if target_info.os == "windows" then
    dso = [[
# undef DSO_DLFCN
# undef HAVE_DLFCN_H
# define DSO_EXTENSION ".dll"
]]
  end
  return {
    ["include/openssl/opensslconf.h"] = [[
#include <openssl/opensslv.h>

#  undef X509_NAME
#  undef X509_EXTENSIONS
#  undef PKCS7_ISSUER_AND_SERIAL
#  undef PKCS7_SIGNER_INFO
#  undef OCSP_REQUEST
#  undef OCSP_RESPONSE

#define OPENSSL_NO_AFALGENG
#define OPENSSL_NO_DEVCRYPTOENG
#define OPENSSL_NO_DYNAMIC_ENGINE
#define OPENSSL_NO_RC5
#define OPENSSL_NO_SCTP

#define NON_EMPTY_TRANSLATION_UNIT static void *dummy = &dummy;

#ifndef DECLARE_DEPRECATED
# define DECLARE_DEPRECATED(f)    f __attribute__ ((deprecated));
#endif

#ifndef OPENSSL_FILE
# define OPENSSL_FILE ""
# define OPENSSL_LINE 0
#endif

#ifndef OPENSSL_MIN_API
# define OPENSSL_MIN_API 0
#endif

#if !defined(OPENSSL_API_COMPAT) || OPENSSL_API_COMPAT < OPENSSL_MIN_API
# undef OPENSSL_API_COMPAT
# define OPENSSL_API_COMPAT OPENSSL_MIN_API
#endif

#if OPENSSL_VERSION_NUMBER < 0x10200000L
# define DEPRECATEDIN_1_2_0(f)   f;
#elif OPENSSL_API_COMPAT < 0x10200000L
# define DEPRECATEDIN_1_2_0(f)   DECLARE_DEPRECATED(f)
#else
# define DEPRECATEDIN_1_2_0(f)
#endif

#if OPENSSL_API_COMPAT < 0x10100000L
# define DEPRECATEDIN_1_1_0(f)   DECLARE_DEPRECATED(f)
#else
# define DEPRECATEDIN_1_1_0(f)
#endif

#if OPENSSL_API_COMPAT < 0x10000000L
# define DEPRECATEDIN_1_0_0(f)   DECLARE_DEPRECATED(f)
#else
# define DEPRECATEDIN_1_0_0(f)
#endif

#if OPENSSL_API_COMPAT < 0x00908000L
# define DEPRECATEDIN_0_9_8(f)   DECLARE_DEPRECATED(f)
#else
# define DEPRECATEDIN_0_9_8(f)
#endif

#undef OPENSSL_UNISTD
#define OPENSSL_UNISTD <unistd.h>

#if !defined(OPENSSL_SYS_UEFI)
# undef BN_LLONG
]] .. bits .. [[
#endif

#define RC4_INT unsigned int
]],

["include/crypto/bn_conf.h"] = [[
#ifndef OSSL_CRYPTO_BN_CONF_H
# define OSSL_CRYPTO_BN_CONF_H
]] .. bits .. [[
#endif
]],

["include/crypto/dso_conf.h"] = [[
#ifndef OSSL_CRYPTO_DSO_CONF_H
# define OSSL_CRYPTO_DSO_CONF_H
]] .. dso .. [[
#endif
]],

["crypto/buildinf.h"] = [[
#define PLATFORM "platform: linux-x86_64"
#define DATE "built on: Wed Nov  3 16:08:13 2021 UTC"
static const char compiler_flags[] = {};
]],

-- could be avoided in OpenSSL 3 (OPENSSL_APPLE_CRYPTO_RANDOM)
["include/CommonCrypto/CommonRandom.h"] = [[
#ifndef CommonCrypto_CommonRandom_h
#define CommonCrypto_CommonRandom_h

static const int kCCSuccess = 0;

static inline int CCRandomGenerateBytes(void *bytes, size_t count) {
  return getentropy(bytes, count);
}

#endif
]],

-- https://github.com/ziglang/zig/issues/7452
["gai_strerror.c"] = [[
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#undef  __CRT__NO_INLINE
#define __CRT__NO_INLINE
#include <stdlib.h>
#include <winsock2.h>
#include <ws2tcpip.h>

char *gai_strerrorA(int ecode) {
  static char buff[GAI_STRERROR_BUFFER_SIZE + 1];
  wcstombs(buff, gai_strerrorW(ecode), GAI_STRERROR_BUFFER_SIZE + 1);
  return buff;
}

WCHAR *gai_strerrorW(int ecode) {
  DWORD dwMsgLen __attribute__((unused));
  static WCHAR buff[GAI_STRERROR_BUFFER_SIZE + 1];
  dwMsgLen = FormatMessageW(
    FORMAT_MESSAGE_FROM_SYSTEM|FORMAT_MESSAGE_IGNORE_INSERTS|FORMAT_MESSAGE_MAX_WIDTH_MASK,
    NULL, ecode, MAKELANGID(LANG_NEUTRAL,SUBLANG_DEFAULT), (LPWSTR)buff,
    GAI_STRERROR_BUFFER_SIZE, NULL);
  return buff;
}
]]
  }
end
