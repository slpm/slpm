const std = @import("std");
const builtin = @import("builtin");

const peak_rss = switch (builtin.os.tag) {
    .windows => struct {
        fn get() u64 {
            var info: std.os.windows.PROCESS_MEMORY_COUNTERS = undefined;
            if (std.os.windows.psapi.GetProcessMemoryInfo(
                std.os.windows.kernel32.GetCurrentProcess(),
                &info,
                @sizeOf(std.os.windows.PROCESS_MEMORY_COUNTERS),
            ) != 0) {
                return info.PeakWorkingSetSize;
            } else {
                return 0;
            }
        }
    },
    .linux, .macos => struct {
        const __time_t = c_long;
        const __suseconds_t = c_long;

        const struct_timeval = extern struct {
            tv_sec: __time_t,
            tv_usec: __suseconds_t,
        };

        const struct_rusage = extern struct {
            ru_utime: struct_timeval,
            ru_stime: struct_timeval,
            ru_maxrss: c_long,
            ru_ixrss: c_long,
            ru_idrss: c_long,
            ru_isrss: c_long,
            ru_minflt: c_long,
            ru_majflt: c_long,
            ru_nswap: c_long,
            ru_inblock: c_long,
            ru_oublock: c_long,
            ru_msgsnd: c_long,
            ru_msgrcv: c_long,
            ru_nsignals: c_long,
            ru_nvcsw: c_long,
            ru_nivcsw: c_long,
        };

        extern "c" fn getrusage(who: c_int, usage: *struct_rusage) c_int;

        fn get() u64 {
            const RUSAGE_SELF = @as(c_int, 0);
            var usage: struct_rusage = undefined;
            if (getrusage(RUSAGE_SELF, &usage) == 0) {
                return @intCast(u64, usage.ru_maxrss * 1024);
            } else {
                return 0;
            }
        }
    },
    else => struct {
        fn get() u64 {
            return 0;
        }
    },
};

const current_rss = switch (builtin.os.tag) {
    .windows => struct {
        fn get() u64 {
            var info: std.os.windows.PROCESS_MEMORY_COUNTERS = undefined;
            if (std.os.windows.psapi.GetProcessMemoryInfo(
                std.os.windows.kernel32.GetCurrentProcess(),
                &info,
                @sizeOf(std.os.windows.PROCESS_MEMORY_COUNTERS),
            ) != 0) {
                return info.WorkingSetSize;
            } else {
                return 0;
            }
        }
    },
    .macos => struct {
        fn get() u64 {
            const c = @cImport({
                @cInclude("mach/mach.h");
            });

            var info: c.mach_task_basic_info = undefined;
            var info_count: c.mach_msg_type_number_t = c.MACH_TASK_BASIC_INFO_COUNT;
            const self = c.mach_task_self();
            if (c.task_info(
                self,
                c.MACH_TASK_BASIC_INFO,
                @ptrCast(c.task_info_t, &info),
                &info_count,
            ) != c.KERN_SUCCESS) {
                return 0;
            } else {
                return info.resident_size;
            }
        }
    },
    .linux => struct {
        fn _get() !c_long {
            var f = try std.fs.openFileAbsolute(
                "/proc/self/statm",
                .{ .intended_io_mode = .blocking },
            );
            defer f.close();

            var buf: [500]u8 = undefined;
            const text = buf[0..try f.reader().readAll(&buf)];
            var iter = std.mem.split(u8, text, " ");
            _ = iter.next(); // size       (1) total program size
            if (iter.next()) |i| { //resident   (2) resident set size
                return try std.fmt.parseInt(c_long, i, 10);
            } else {
                return 0;
            }
        }

        fn get() u64 {
            const c = @cImport({
                @cInclude("unistd.h");
            });

            return @intCast(u64, (_get() catch 0) * c.sysconf(c._SC_PAGESIZE));
        }
    },
    else => struct {
        fn get() u64 {
            return 0;
        }
    },
};

export fn getPeakRSS() u64 {
    return peak_rss.get();
}

export fn getCurrentRSS() u64 {
    return current_rss.get();
}
