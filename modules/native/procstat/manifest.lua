local build

name = "procstat"
description = "Process statistics"

versions = {
  {
    version = "0.0.1",
    build = function(cfg, api)
      return build(cfg, api)
    end,
  }
}

build = function(cfg, api)
  local extern_name = string.format(
    "%s-%s.zig", cfg.pkg.name, cfg.pkg.version)

  if not cfg.args.dry_run then
    api.install(
      tonumber("0644", 8),
      api.path.join(cfg.pkg.dir, "extern.zig"),
      cfg.prefix.zigpkgdir,
      extern_name)
  end

  local lib_name = string.format(
    "%s-%s-%s", cfg.pkg.name, cfg.args.mode, cfg.pkg.version)
  local options = {
    lib = {
      ["native/procstat"] = {
        name = lib_name,
        object = api.get_lib_object(cfg, lib_name),
        version = cfg.pkg.version,
        zigpkg = api.path.join(cfg.prefix.zigpkgdir, extern_name),
      }
    },
    prefix = cfg.prefix,
    -- entries below are not required for dependencies
    mode = cfg.args.mode,
    target = cfg.args.target,
  }

  api.zig_build(cfg, string.format("-Doptions=%q", api.json_encode(options)))

  return {
    lib = options.lib,
    prefix = options.prefix,
  }
end
