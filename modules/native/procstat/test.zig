const std = @import("std");
const procstat = @import("native/procstat");

test "getPeakRSS" {
    const rss = procstat.getPeakRSS();
    try std.testing.expect(rss > 0);
}

test "getCurrentRSS" {
    const rss = procstat.getCurrentRSS();
    try std.testing.expect(rss > 0);
}
