local build

name = "curl"
description = "Library for transferring data with URLs"
releases = "https://curl.se/download.html"

versions = {
  {
    version = "7.79.1",
    build = function(cfg, api)
      local sha1 = "ed887365eb7d45755404a94933bad58976641ab0"
      return build(cfg, api, sha1)
    end,
  }
}

build = function(cfg, api, archive_sha1)
  local curl_version = "curl-" .. cfg.pkg.version
  local url_root = "https://curl.se/download/"

  local origin
  if not cfg.args.dry_run then
    origin = api.fetch(
      cfg, {
        url = url_root .. curl_version .. ".tar.xz",
        name = curl_version .. ".tar.xz",
        subdir = curl_version,
        hash = {
          fn = "sha1",
          digest = archive_sha1
        }
    })

    local includes = {
      "curl.h",
      "curlver.h",
      "easy.h",
      "mprintf.h",
      "stdcheaders.h",
      "multi.h",
      "typecheck-gcc.h",
      "system.h",
      "urlapi.h",
      "options.h",
    }
    for _, i in ipairs(includes) do
      api.install(
        tonumber("0644", 8),
        api.path.join(origin, "include", "curl", i),
        api.path.join(cfg.prefix.includedir, "curl"))
    end
  end

  local lib_name = string.format(
    "%s-%s-%s", cfg.pkg.name, cfg.args.mode, cfg.pkg.version)
  local options = {
    lib = {
      ["native/curl"] = {
        name = lib_name,
        object = api.get_lib_object(cfg, lib_name),
        version = cfg.pkg.version,
      }
    },
    prefix = cfg.prefix,
    -- entries below are not required for dependencies
    mode = cfg.args.mode,
    origin = origin,
    target = cfg.args.target,
  }

  api.zig_build(cfg, string.format("-Doptions=%q", api.json_encode(options)))

  return {
    lib = options.lib,
    prefix = options.prefix,
  }
end
