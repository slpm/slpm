pub fn build(b: *Builder) !void {
    const opts = try getOptions(b);

    const cflags = [_][]const u8{};
    const c_files = [_][]const u8{
        "lib/altsvc.c",
        "lib/amigaos.c",
        "lib/asyn-ares.c",
        "lib/asyn-thread.c",
        "lib/base64.c",
        "lib/bufref.c",
        "lib/c-hyper.c",
        "lib/conncache.c",
        "lib/connect.c",
        "lib/content_encoding.c",
        "lib/cookie.c",
        "lib/curl_addrinfo.c",
        "lib/curl_ctype.c",
        "lib/curl_des.c",
        "lib/curl_endian.c",
        "lib/curl_fnmatch.c",
        "lib/curl_get_line.c",
        "lib/curl_gethostname.c",
        "lib/curl_gssapi.c",
        "lib/curl_memrchr.c",
        "lib/curl_multibyte.c",
        "lib/curl_ntlm_core.c",
        "lib/curl_ntlm_wb.c",
        "lib/curl_path.c",
        "lib/curl_range.c",
        "lib/curl_rtmp.c",
        "lib/curl_sasl.c",
        "lib/curl_sspi.c",
        "lib/curl_threads.c",
        "lib/dict.c",
        "lib/doh.c",
        "lib/dotdot.c",
        "lib/dynbuf.c",
        "lib/easy.c",
        "lib/easygetopt.c",
        "lib/easyoptions.c",
        "lib/escape.c",
        "lib/file.c",
        "lib/fileinfo.c",
        "lib/formdata.c",
        "lib/ftp.c",
        "lib/ftplistparser.c",
        "lib/getenv.c",
        "lib/getinfo.c",
        "lib/gopher.c",
        "lib/hash.c",
        "lib/hmac.c",
        "lib/hostasyn.c",
        "lib/hostcheck.c",
        "lib/hostip.c",
        "lib/hostip4.c",
        "lib/hostip6.c",
        "lib/hostsyn.c",
        "lib/hsts.c",
        "lib/http.c",
        "lib/http2.c",
        "lib/http_aws_sigv4.c",
        "lib/http_chunks.c",
        "lib/http_digest.c",
        "lib/http_negotiate.c",
        "lib/http_ntlm.c",
        "lib/http_proxy.c",
        "lib/idn_win32.c",
        "lib/if2ip.c",
        "lib/imap.c",
        "lib/inet_ntop.c",
        "lib/inet_pton.c",
        "lib/krb5.c",
        "lib/llist.c",
        "lib/md4.c",
        "lib/md5.c",
        "lib/memdebug.c",
        "lib/mime.c",
        "lib/mprintf.c",
        "lib/mqtt.c",
        "lib/multi.c",
        "lib/netrc.c",
        "lib/non-ascii.c",
        "lib/nonblock.c",
        "lib/openldap.c",
        "lib/parsedate.c",
        "lib/pingpong.c",
        "lib/pop3.c",
        "lib/progress.c",
        "lib/psl.c",
        "lib/rand.c",
        "lib/rename.c",
        "lib/rtsp.c",
        "lib/select.c",
        "lib/sendf.c",
        "lib/setopt.c",
        "lib/sha256.c",
        "lib/share.c",
        "lib/slist.c",
        "lib/smb.c",
        "lib/smtp.c",
        "lib/socketpair.c",
        "lib/socks.c",
        "lib/socks_gssapi.c",
        "lib/socks_sspi.c",
        "lib/speedcheck.c",
        "lib/splay.c",
        "lib/strcase.c",
        "lib/strdup.c",
        "lib/strerror.c",
        "lib/strtok.c",
        "lib/strtoofft.c",
        "lib/system_win32.c",
        "lib/telnet.c",
        "lib/tftp.c",
        "lib/timeval.c",
        "lib/transfer.c",
        "lib/url.c",
        "lib/urlapi.c",
        "lib/version.c",
        "lib/version_win32.c",
        "lib/warnless.c",
        "lib/wildcard.c",
        "lib/x509asn1.c",
        "lib/vtls/vtls.c",
    };

    const lib = b.addStaticLibrary(getLibName("native/curl", opts), null);
    setBuildMode(lib, opts);
    setLibOutputDir(lib, opts);
    addCSourceFiles(b, lib, c_files[0..], cflags[0..], opts);

    lib.linkLibC();
    lib.setTarget(opts.target);

    const macros = [_]Str{
        "BUILDING_LIBCURL",
        "CURL_DISABLE_CRYPTO_AUTH",
        "CURL_DISABLE_IMAP",
        "CURL_DISABLE_LDAP",
        "CURL_DISABLE_POP3",
        "CURL_DISABLE_SMTP",
        "CURL_DISABLE_SOCKETPAIR",
        "CURL_STATICLIB",
        "HAVE_ERRNO_H",
        "HAVE_FCNTL_H",
        "HAVE_LONGLONG",
        "HAVE_SELECT",
        "HAVE_SOCKET",
        "HAVE_STRUCT_TIMEVAL",
        "HAVE_SYS_STAT_H",
        "HAVE_UNISTD_H",
    };
    for (macros) |macro| {
        lib.defineCMacro(macro, null);
    }

    if (opts.os_tag != .windows) {
        lib.defineCMacro("HAVE_ARPA_INET_H", null);
        lib.defineCMacro("HAVE_FCNTL_O_NONBLOCK", null);
        lib.defineCMacro("HAVE_NETDB_H", null);
        lib.defineCMacro("HAVE_SYS_SOCKET_H", null);
    }

    lib.defineCMacro("HAVE_RECV", null);
    lib.defineCMacro("RECV_TYPE_ARG1", "int");
    lib.defineCMacro("RECV_TYPE_ARG2", "void*");
    lib.defineCMacro("RECV_TYPE_ARG3", "size_t");
    lib.defineCMacro("RECV_TYPE_ARG4", "int");
    lib.defineCMacro("RECV_TYPE_RETV", "ssize_t");

    lib.defineCMacro("HAVE_SEND", null);
    lib.defineCMacro("SEND_TYPE_ARG1", "int");
    lib.defineCMacro("SEND_QUAL_ARG2", "const");
    lib.defineCMacro("SEND_TYPE_ARG2", "void*");
    lib.defineCMacro("SEND_TYPE_ARG3", "size_t");
    lib.defineCMacro("SEND_TYPE_ARG4", "int");
    lib.defineCMacro("SEND_TYPE_RETV", "ssize_t");

    lib.defineCMacro("SIZEOF_CURL_OFF_T", "8");
    const triple = try opts.target.linuxTriple(b.allocator);
    lib.defineCMacro("OS", b.fmt("\"{s}\"", .{triple}));

    lib.addIncludeDir(normalized(b, opts.origin, "include"));
    lib.addIncludeDir(normalized(b, opts.origin, "lib"));
    lib.install();
    b.default_step.dependOn(&lib.step);

    const test_exe = b.addTest("test.zig");
    addObjectFiles(test_exe, opts);
    addIncludeDirs(test_exe, opts);

    test_exe.linkLibC();
    if (opts.os_tag == .windows) {
        test_exe.linkSystemLibrary("ws2_32");
    }

    test_exe.setTarget(opts.target);
    test_exe.enable_qemu = true;
    test_exe.enable_darling = true;
    test_exe.enable_wine = true;
    test_exe.step.dependOn(&lib.step);

    const test_step = b.step("test", "Test the library");
    test_step.dependOn(&test_exe.step);
}

// BEGIN GENERATED CONTENT
const std = @import("std");

const Builder = std.build.Builder;
const CrossTarget = std.zig.CrossTarget;
const LibExeObjStep = std.build.LibExeObjStep;
const Mode = std.builtin.Mode;
const ObjectMap = std.json.ObjectMap;
const Str = []const u8;
const Target = std.Target;

const Options = struct {
    bindir: ?Str,
    cpu_arch: Target.Cpu.Arch,
    cpu_bits: usize,
    cpu_features: Target.Cpu.Feature.Set,
    libdir: ?Str,
    mode: Mode,
    obj: ObjectMap,
    origin: ?Str,
    os_tag: Target.Os.Tag,
    target: CrossTarget,
};

fn addCSourceFiles(
    b: *Builder,
    artifact: *LibExeObjStep,
    files: []const Str,
    flags: []const Str,
    opts: Options,
) void {
    for (files) |c_file| {
        artifact.addCSourceFile(normalized(b, opts.origin, c_file), flags);
    }
}

fn addIncludeDirs(artifact: *LibExeObjStep, opts: Options) void {
    const add = (struct {
        artifact: *LibExeObjStep,
        fn add(self: @This(), obj: ObjectMap) void {
            if (obj.get("includedir")) |dir| {
                self.artifact.addIncludeDir(dir.String);
            }
        }
    }{ .artifact = artifact }).add;

    if (opts.obj.get("dep_opts")) |dep_opts| {
        if (dep_opts.Object.get("prefix")) |prefix| {
            for (prefix.Array.items) |v| {
                add(v.Object);
            }
        }
    }

    if (opts.obj.get("prefix")) |prefix| {
        add(prefix.Object);
    }
}

fn addObjectFiles(artifact: *LibExeObjStep, opts: Options) void {
    const add = (struct {
        artifact: *LibExeObjStep,
        fn add(self: @This(), obj: ObjectMap) void {
            var it = obj.iterator();
            while (it.next()) |raw_entry| {
                const entry = raw_entry.value_ptr.*;
                if (entry.Object.get("object")) |object| {
                    self.artifact.addObjectFile(object.String);
                }
            }
        }
    }{ .artifact = artifact }).add;

    if (opts.obj.get("dep_opts")) |dep_opts| {
        if (dep_opts.Object.get("lib")) |lib| {
            for (lib.Array.items) |v| {
                add(v.Object);
            }
        }
    }

    if (opts.obj.get("lib")) |lib| {
        add(lib.Object);
    }
}

fn addPackagePaths(artifact: *LibExeObjStep, opts: Options) void {
    const add = (struct {
        artifact: *LibExeObjStep,
        fn add(self: @This(), obj: ObjectMap) void {
            var it = obj.iterator();
            while (it.next()) |raw_entry| {
                const key = raw_entry.key_ptr.*;
                const entry = raw_entry.value_ptr.*;
                if (entry.Object.get("zigpkg")) |object| {
                    self.artifact.addPackagePath(key, object.String);
                }
            }
        }
    }{ .artifact = artifact }).add;

    if (opts.obj.get("dep_opts")) |dep_opts| {
        if (dep_opts.Object.get("lib")) |lib| {
            for (lib.Array.items) |v| {
                add(v.Object);
            }
        }
    }

    if (opts.obj.get("lib")) |lib| {
        add(lib.Object);
    }
}

fn getBinName(selector: Str, opts: Options) Str {
    if (opts.obj.get("bin")) |binobj| {
        if (binobj.Object.get(selector)) |main| {
            if (main.Object.get("name")) |v| {
                return v.String;
            }
        }
    }
    return "bin";
}

fn getLibName(selector: Str, opts: Options) Str {
    if (opts.obj.get("lib")) |libobj| {
        if (libobj.Object.get(selector)) |main| {
            if (main.Object.get("name")) |v| {
                return v.String;
            }
        }
    }
    return "lib";
}

fn getOptions(b: *Builder) !Options {
    var target = b.standardTargetOptions(.{});
    var mode = b.standardReleaseOptions();

    var opt_origin: ?Str = null;
    var opt_libdir: ?Str = null;
    var opt_bindir: ?Str = null;

    var options_obj = std.json.ObjectMap.init(b.allocator);
    const opt_options = b.option(
        Str,
        "options",
        "Project options in JSON format",
    );
    if (opt_options) |text| {
        var p = std.json.Parser.init(b.allocator, false);
        defer p.deinit();

        options_obj = (try p.parse(text)).root.Object;
        if (options_obj.get("origin")) |origin| {
            opt_origin = origin.String;
        }
        if (options_obj.get("prefix")) |v| {
            if (v.Object.get("libdir")) |libdir| {
                opt_libdir = libdir.String;
            }
            if (v.Object.get("bindir")) |bindir| {
                opt_bindir = bindir.String;
            }
        }
    }

    if (options_obj.get("target")) |v| {
        target = try CrossTarget.parse(.{ .arch_os_abi = v.String });
    }

    if (options_obj.get("mode")) |v| {
        mode = if (std.mem.eql(u8, v.String, "safe"))
            Mode.ReleaseSafe
        else if (std.mem.eql(u8, v.String, "fast"))
            Mode.ReleaseFast
        else if (std.mem.eql(u8, v.String, "small"))
            Mode.ReleaseSmall
        else
            Mode.Debug;
    }

    const target_info = try std.zig.system.NativeTargetInfo.detect(
        b.allocator,
        target,
    );
    const resolved_target = target_info.target;
    const arch = resolved_target.cpu.arch;
    const features = resolved_target.cpu.features;
    const os_tag = resolved_target.os.tag;

    return Options{
        .bindir = opt_bindir,
        .cpu_arch = arch,
        .cpu_bits = arch.ptrBitWidth(),
        .cpu_features = features,
        .libdir = opt_libdir,
        .mode = mode,
        .obj = options_obj,
        .origin = opt_origin,
        .os_tag = os_tag,
        .target = target,
    };
}

fn normalized(b: *Builder, opt_prefix: ?Str, path: Str) Str {
    if (opt_prefix) |prefix| {
        return b.fmt("{s}/{s}", .{ prefix, path });
    } else {
        return path;
    }
}

fn setBuildMode(artifact: *LibExeObjStep, opts: Options) void {
    artifact.setBuildMode(opts.mode);
    if (opts.mode == .ReleaseSmall) {
        artifact.strip = true;
    }
}

fn setLibOutputDir(artifact: *LibExeObjStep, opts: Options) void {
    if (opts.libdir) |libdir| {
        artifact.setOutputDir(libdir);
    }
}

fn setBinOutputDir(artifact: *LibExeObjStep, opts: Options) void {
    if (opts.bindir) |bindir| {
        artifact.setOutputDir(bindir);
    }
}
// END GENERATED CONTENT
