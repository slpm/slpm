const std = @import("std");
const c = @cImport({
    @cInclude("curl/curl.h");
});

test "curl_easy_init" {
    const h = c.curl_easy_init();
    try std.testing.expect(h != null);
    defer c.curl_easy_cleanup(h);
}

test "curl_url" {
    // get a handle to work with
    const h = c.curl_url();
    try std.testing.expect(h != null);
    defer c.curl_url_cleanup(h);

    // parse a full URL
    try std.testing.expectEqual(c.curl_url_set(
        h,
        c.CURLUPART_URL,
        "http://example.com/path/index.html",
        0,
    ), c.CURLUE_OK);

    // extract host name from the parsed URL
    var host: [*c]u8 = undefined;
    try std.testing.expectEqual(
        c.curl_url_get(h, c.CURLUPART_HOST, &host, 0),
        c.CURLUE_OK,
    );
    defer c.curl_free(host);
    try std.testing.expectEqualStrings(std.mem.span(host), "example.com");

    // extract the path from the parsed URL
    var path: [*c]u8 = undefined;
    try std.testing.expectEqual(
        c.curl_url_get(h, c.CURLUPART_PATH, &path, 0),
        c.CURLUE_OK,
    );
    try std.testing.expectEqualStrings(std.mem.span(path), "/path/index.html");
    c.curl_free(path);

    // redirect with a relative URL
    try std.testing.expectEqual(
        c.curl_url_set(h, c.CURLUPART_URL, "../another/second.html", 0),
        c.CURLUE_OK,
    );

    // extract the new, updated path
    try std.testing.expectEqual(
        c.curl_url_get(h, c.CURLUPART_PATH, &path, 0),
        c.CURLUE_OK,
    );
    try std.testing.expectEqualStrings(
        std.mem.span(path),
        "/another/second.html",
    );
    c.curl_free(path);
}
