Lua::	5.4.3

|===
|Mode |File size |RSS |Run time

|ReleaseFast
|1.50MiB
|287.41MiB
|1.959s

|ReleaseSmall
|267.88KiB
|287.30MiB
|2.056s

|ReleaseSafe
|2.42MiB
|287.71MiB
|3.685s

|Debug
|2.43MiB
|287.70MiB
|4.184s

|===
