local sep, other_sep, seps

local path = {
  sep = '/'
}
sep = path.sep

--- given a path, return the directory part and a file part.
-- if there's no directory part, the first value will be empty
-- @string P A file path
-- @return directory part
-- @return file part
-- @usage
-- local dir, file = path.splitpath("some/dir/myfile.txt")
-- assert(dir == "some/dir")
-- assert(file == "myfile.txt")
--
-- local dir, file = path.splitpath("some/dir/")
-- assert(dir == "some/dir")
-- assert(file == "")
--
-- local dir, file = path.splitpath("some_dir")
-- assert(dir == "")
-- assert(file == "some_dir")
local function splitpath(p1)
    local i = #p1
    local ch = p1:sub(i, i)
    while i > 0 and ch ~= sep do
        i = i - 1
        ch = p1:sub(i, i)
    end

    if i == 0 then
        return '', p1
    else
        return p1:sub(1, i - 1), p1:sub(i + 1)
    end
end

function path.join(p1, p2, ...)
  if select('#', ...) > 0 then
    local p = path.join(p1,p2)
    local args = {...}
    for i = 1, #args do
      p = path.join(p, args[i])
    end
    return p
  end
  local endc = p1:sub(-1)
  if endc ~= path.sep and endc ~= other_sep and endc ~= "" then
    p1 = p1..path.sep
  end
  return p1..p2
end

function path.dirname(p1)
  local p1 = splitpath(p1)
  return p1
end

function path.basename(p1)
  local _, p2 = splitpath(p1)
  return p2
end

function path.tmpdir()
  local exampleTempFilePath = os.tmpname()

  -- remove generated temp file
  pcall(os.remove, exampleTempFilePath)

  local separatorIdx = exampleTempFilePath:reverse():find(sep)
  local tempPathStringLength = #exampleTempFilePath - separatorIdx

  return exampleTempFilePath:sub(1, tempPathStringLength)
end

return path
