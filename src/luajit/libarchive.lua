local bit = require "bit"
local ffi = require "ffi"

local cwd = (...):match("(.-)[^%.]+$")
local libc = require(cwd .. "libc")

local archive = ffi.load("archive")

ffi.cdef[[
struct archive;
struct archive_entry;
const char *archive_error_string(struct archive *);

struct archive *archive_read_new(void);
int archive_read_free(struct archive *);

struct archive *archive_write_disk_new(void);
int archive_write_free(struct archive *);

int archive_read_support_filter_all(struct archive *);
int archive_read_support_format_all(struct archive *);
int archive_read_support_format_tar(struct archive *);
int archive_read_support_filter_gzip(struct archive *);

int archive_write_disk_set_options(struct archive *, int flags);
int archive_write_disk_set_standard_lookup(struct archive *);
int archive_read_open_filename(struct archive *, const char *filename, size_t block_size);
int archive_read_next_header(struct archive *, struct archive_entry **);
int archive_write_header(struct archive *, struct archive_entry *);

int64_t archive_entry_size(struct archive_entry *a);

long archive_read_data(struct archive *, void *buff, size_t len);
int archive_read_data_block(struct archive *, const void **buff, size_t *len, int64_t *offset);
long archive_write_data(struct archive *, const void *, size_t size);
long archive_write_data_block(struct archive *, const void *, size_t size, int64_t offset);
int archive_write_finish_entry(struct archive *);
]]

-- Error codes: Use archive_errno() and archive_error_string()
-- to retrieve details.  Unless specified otherwise, all functions
-- that return 'int' use these codes.

local ARCHIVE_EOF = 1  -- Found end of archive.
local ARCHIVE_OK = 0  --  Operation was successful.
local ARCHIVE_RETRY = -10  -- Retry might succeed.
local ARCHIVE_WARN = -20  -- Partial success.
-- For example, if write_header "fails", then you can't push data.
local ARCHIVE_FAILED = -25  -- Current operation cannot complete.
-- But if write_header is "fatal," then this archive is dead and useless.
local ARCHIVE_FATAL = -30  -- No more operations are possible.

-- The "flags" argument selects optional behavior, 'OR' the flags you want.

-- Default: Do not try to set owner/group
local ARCHIVE_EXTRACT_OWNER = 0x0001
-- Default: Do obey umask, do not restore SUID/SGID/SVTX bits.
local ARCHIVE_EXTRACT_PERM = 0x0002
-- Default: Do not restore mtime/atime.
local ARCHIVE_EXTRACT_TIME = 0x0004
-- Default: Replace existing files.
local ARCHIVE_EXTRACT_NO_OVERWRITE = 0x0008
-- Default: Try create first, unlink only if create fails with EEXIST.
local ARCHIVE_EXTRACT_UNLINK = 0x0010
-- Default: Do not restore ACLs.
local ARCHIVE_EXTRACT_ACL = 0x0020
-- Default: Do not restore fflags.
local ARCHIVE_EXTRACT_FFLAGS = 0x0040
-- Default: Do not restore xattrs.
local ARCHIVE_EXTRACT_XATTR = 0x0080
-- Default: Do not try to guard against extracts redirected by symlinks.
-- Note: With ARCHIVE_EXTRACT_UNLINK, will remove any intermediate symlink.
local ARCHIVE_EXTRACT_SECURE_SYMLINKS = 0x0100
-- Default: Do not reject entries with '..' as path elements.
local ARCHIVE_EXTRACT_SECURE_NODOTDOT = 0x0200
-- Default: Create parent directories as needed.
local ARCHIVE_EXTRACT_NO_AUTODIR = 0x0400
-- Default: Overwrite files, even if one on disk is newer.
local ARCHIVE_EXTRACT_NO_OVERWRITE_NEWER = 0x0800
-- Detect blocks of 0 and write holes instead.
local ARCHIVE_EXTRACT_SPARSE = 0x1000
-- Default: Do not restore Mac extended metadata.
-- This has no effect except on Mac OS.
local ARCHIVE_EXTRACT_MAC_METADATA = 0x2000
-- Default: Use HFS+ compression if it was compressed.
-- This has no effect except on Mac OS v10.6 or later.
local ARCHIVE_EXTRACT_NO_HFS_COMPRESSION = 0x4000
-- Default: Do not use HFS+ compression if it was not compressed.
-- This has no effect except on Mac OS v10.6 or later.
local ARCHIVE_EXTRACT_HFS_COMPRESSION_FORCED = 0x8000
-- Default: Do not reject entries with absolute paths
local ARCHIVE_EXTRACT_SECURE_NOABSOLUTEPATHS = 0x10000
-- Default: Do not clear no-change flags when unlinking object
local ARCHIVE_EXTRACT_CLEAR_NOCHANGE_FFLAGS = 0x20000
-- Default: Do not extract atomically (using rename)
local ARCHIVE_EXTRACT_SAFE_WRITES = 0x40000

local function archive_error_string(a)
  return ffi.string(archive.archive_error_string(a))
end

local function archive_error(a)
  error(archive_error_string(a))
end

local function handle_error(a, r)
  if r < ARCHIVE_OK then
    io.stderr:write(archive_error_string(a) .. "\n")
  end
  if r < ARCHIVE_WARN then
    error(archive_error_string(a))
  end
end

local function copy_data(ar, aw)
  local SIZE = 2^16
  local buff = ffi.new("char[?]", SIZE)

  while true do
    local r = archive.archive_read_data(ar, buff, SIZE)
    if r == 0 then
      return ARCHIVE_OK
    elseif r > 0 then
        r = archive.archive_write_data(aw, buff, r)
        if r < ARCHIVE_OK then
          archive_error(aw)
        end
    else
      return r
    end
  end
end

local function extract(file, path, progress_cb)
  local cwd = libc.cwd()
  if path then
    libc.cd(path)
  end

  local a = archive.archive_read_new()
  if not a then
    error("archive_read_new() failed")
  end
  ffi.gc(a, archive.archive_read_free)

  if archive.archive_read_support_filter_all(a) ~= ARCHIVE_OK then
    archive_error(a)
  end

  if archive.archive_read_support_format_all(a) ~= ARCHIVE_OK then
    archive_error(a)
  end

  local ext = archive.archive_write_disk_new();
  if not ext then
    error("archive_write_disk_new() failed")
  end
  ffi.gc(a, archive.archive_write_free)

  local flags = bit.bor(ARCHIVE_EXTRACT_TIME, ARCHIVE_EXTRACT_NO_OVERWRITE)
  if archive.archive_write_disk_set_options(ext, flags) ~= ARCHIVE_OK then
    archive_error(a)
  end

  if archive.archive_write_disk_set_standard_lookup(ext) ~= ARCHIVE_OK then
    archive_error(a)
  end

  if archive.archive_read_open_filename(a, file, 2^14) ~= ARCHIVE_OK then
    archive_error(a)
  end

  local entry = ffi.new("struct archive_entry*[1]")
  while true do
    local r = archive.archive_read_next_header(a, entry)
    if r == ARCHIVE_EOF then
      break
    end
    handle_error(a, r)

    handle_error(ext, archive.archive_write_header(ext, entry[0]))

    if archive.archive_entry_size(entry[0]) > 0 then
      handle_error(ext, copy_data(a, ext))
    end

    progress_cb()
  end
  libc.cd(cwd)
end

return {
  extract = extract,
}
