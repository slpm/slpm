local ffi = require "ffi"

ffi.cdef[[
char* getcwd(char *buffer, int maxlen);
int chdir(const char* path);
char *realpath(const char *name, char *resolved);
int access(const char *path, int amode);
char *dirname(char *path);
char *basename(char *path);

typedef struct {
  long unsigned int gl_pathc;
  char **gl_pathv;
  long unsigned int gl_offs;
  int gl_flags;
  void (*gl_closedir)(void *);
  void *(*gl_readdir)(void *);
  void *(*gl_opendir)(const char *);
  int (*gl_lstat)(const char *restrict, void *restrict);
  int (*gl_stat)(const char *restrict, void *restrict);
} glob_t;
int glob(
  const char *pattern,
  int flag,
  int (*)(const char *, int),
  glob_t *pglob);
void globfree(glob_t *pglob);

typedef unsigned int mode_t;
int mkdir(const char *path, mode_t mode);
int chmod(const char *pathname, mode_t mode);

int *__errno_location (void);
char *strerror(int errnum);
]]

local MAX_PATH = 4096

local function cd(dir)
  return ffi.C.chdir(dir) == 0
end

local function cwd()
  local path_buffer = ffi.new("char[?]", MAX_PATH)
  return ffi.string(ffi.C.getcwd(path_buffer, MAX_PATH))
end

local function libc_error()
  local errnol = ffi.C.__errno_location()
  error(ffi.string(ffi.C.strerror(errnol[0])), 2)
end

local function strict_realpath(path)
  local path_buffer = ffi.new("char[?]", MAX_PATH)
  if not ffi.C.realpath(path, path_buffer) then
    libc_error()
  end
  return ffi.string(path_buffer)
end

local function exists(path)
  return ffi.C.access(path, 0) == 0
end

local function realpath(path)
  local currentpath = ""
  local first = path:sub(1, 1) ~= "/"
  for token in path:gmatch("[^/]+") do
    if first then
      currentpath = token
      first = false
    else
      currentpath = currentpath .. "/" .. token
    end
    local path = strict_realpath(currentpath)
    if exists(path) then
      currentpath = path
    end
  end
  return currentpath
end

local function mkdir(path)
  local fullpath = realpath(path) .. "/"
  local currentpath = ""
  for token in fullpath:gmatch("[^/]+") do
    currentpath = currentpath .. "/" .. token
    if not exists(currentpath) then
      -- libc automatically masks permissions
      if not ffi.C.mkdir(currentpath, tonumber("0777", 8)) then
        libc_error()
      end
    end
  end
end

local function dirname(path)
  local c_str = ffi.new("char[?]", #path + 1)
  ffi.copy(c_str, path)
  return ffi.string(ffi.C.dirname(c_str))
end

local function basename(path)
  local c_str = ffi.new("char[?]", #path + 1)
  ffi.copy(c_str, path)
  return ffi.string(ffi.C.basename(c_str))
end

local function chmod(path, mode)
  if not ffi.C.chmod(path, mode) then
    libc_error()
  end
end

function glob(pattern)
  local glob_t = ffi.gc(ffi.new("glob_t[1]"), ffi.C.globfree)
  if not ffi.C.glob(pattern, 0, nil, glob_t) then
    libc_error()
  end

  local files = {}
  local i = 0
  while i < glob_t[0].gl_pathc do
    table.insert(files, ffi.string(glob_t[0].gl_pathv[i]))
    i = i + 1
  end
  return files
end

return {
  basename = basename,
  cd = cd,
  chmod = chmod,
  cwd = cwd,
  dirname = dirname,
  exists = exists,
  glob = glob,
  mkdir = mkdir,
  realpath = realpath,
}
