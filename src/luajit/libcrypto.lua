local bit = require "bit"
local ffi = require "ffi"

local cwd = (...):match("(.-)[^%.]+$")
local libc = require(cwd .. "libc")

local crypto
if ffi.os == "OSX" then
  -- Can't use the default library, and the working values are taken from
  -- https://github.com/cl-plus-ssl/cl-plus-ssl/blob/master/src/reload.lisp
  crypto_paths = {
    "/opt/local/lib/libcrypto.dylib", -- MacPorts
    "/sw/lib/libcrypto.dylib", -- Fink
    "/usr/local/opt/openssl/lib/libcrypto.dylib", -- Homebrew
    "/opt/homebrew/opt/openssl/lib/libcrypto.dylib", -- Homebrew Arm64
    "/usr/local/lib/libcrypto.dylib", -- personalized install

    -- System-provided libraries. Must be loaded from files with
    -- names that include version explicitly, instead of any versionless
    -- symlink file. Otherwise macOS crushes the process (starting from
    -- macOS > 10.15 that was just a warning, and finally macOS >= 11
    -- crashes the process with a fatal error)

    -- Please note that in macOS >= 11.0, these paths may not exist in the
    -- file system anymore, but trying to load them via dlopen will work. This
    -- is because macOS ships all system-provided libraries as a single
    -- dyld_shared_cache bundle.
    "/usr/lib/libcrypto.44.dylib",
    "/usr/lib/libcrypto.42.dylib",
    "/usr/lib/libcrypto.41.dylib",
    "/usr/lib/libcrypto.35.dylib",
  }
  for _, path in ipairs(crypto_paths) do
    if libc.exists(path) then
      crypto = ffi.load(path)
      break
    end
  end
else
  crypto = ffi.load("crypto")
end

ffi.cdef[[
typedef struct bio_st BIO;
typedef struct bio_method_st BIO_METHOD;
BIO_METHOD *BIO_s_mem(void);
BIO * BIO_new(BIO_METHOD *type);
int BIO_puts(BIO *bp, const char *buf);
void BIO_vfree(BIO *a);

typedef struct rsa_st RSA;
RSA *RSA_new(void);
void RSA_free(RSA *rsa);
typedef int pem_password_cb(char *buf, int size, int rwflag, void *userdata);
RSA * PEM_read_bio_RSAPrivateKey(BIO *bp, RSA **rsa, pem_password_cb *cb,
                                 void *u);
RSA * PEM_read_bio_RSAPublicKey(BIO *bp, RSA **rsa, pem_password_cb *cb,
                                void *u);
RSA * PEM_read_bio_RSA_PUBKEY(BIO *bp, RSA **rsa, pem_password_cb *cb,
                                void *u);

unsigned long ERR_get_error_line_data(const char **file, int *line,
                                      const char **data, int *flags);
const char * ERR_reason_error_string(unsigned long e);

typedef struct bignum_st BIGNUM;
BIGNUM *BN_new(void);
void BN_free(BIGNUM *a);
typedef unsigned long BN_ULONG;
int BN_set_word(BIGNUM *a, BN_ULONG w);
typedef struct bn_gencb_st BN_GENCB;
int RSA_generate_key_ex(RSA *rsa, int bits, BIGNUM *e, BN_GENCB *cb);

typedef struct evp_cipher_st EVP_CIPHER;
int PEM_write_bio_RSAPrivateKey(BIO *bp, RSA *x, const EVP_CIPHER *enc,
                                unsigned char *kstr, int klen,
                                pem_password_cb *cb, void *u);
int PEM_write_bio_RSAPublicKey(BIO *bp, RSA *x);
int PEM_write_bio_RSA_PUBKEY(BIO *bp, RSA *x);

long BIO_ctrl(BIO *bp, int cmd, long larg, void *parg);
int BIO_read(BIO *b, void *data, int len);

typedef struct evp_pkey_st EVP_PKEY;
typedef struct engine_st ENGINE;
typedef struct evp_pkey_ctx_st EVP_PKEY_CTX;

EVP_PKEY *EVP_PKEY_new(void);
void EVP_PKEY_free(EVP_PKEY *key);

EVP_PKEY_CTX *EVP_PKEY_CTX_new(EVP_PKEY *pkey, ENGINE *e);
void EVP_PKEY_CTX_free(EVP_PKEY_CTX *ctx);

int EVP_PKEY_CTX_ctrl(EVP_PKEY_CTX *ctx, int keytype, int optype,
                      int cmd, int p1, void *p2);

int EVP_PKEY_size(EVP_PKEY *pkey);

int EVP_PKEY_encrypt_init(EVP_PKEY_CTX *ctx);
int EVP_PKEY_encrypt(EVP_PKEY_CTX *ctx,
        unsigned char *out, size_t *outlen,
        const unsigned char *in, size_t inlen);

int EVP_PKEY_decrypt_init(EVP_PKEY_CTX *ctx);
int EVP_PKEY_decrypt(EVP_PKEY_CTX *ctx,
                     unsigned char *out, size_t *outlen,
                     const unsigned char *in, size_t inlen);

int EVP_PKEY_set1_RSA(EVP_PKEY *pkey, RSA *key);
int PEM_write_bio_PKCS8PrivateKey(BIO *bp, EVP_PKEY *x, const EVP_CIPHER *enc,
                                  char *kstr, int klen, pem_password_cb *cb,
                                  void *u);

typedef struct env_md_st EVP_MD;
typedef struct env_md_ctx_st EVP_MD_CTX;

void OpenSSL_add_all_digests(void);
const EVP_MD *EVP_get_digestbyname(const char *name);

EVP_MD_CTX *EVP_MD_CTX_new(void);
void EVP_MD_CTX_free(EVP_MD_CTX *ctx);

int EVP_DigestInit_ex(EVP_MD_CTX *ctx, const EVP_MD *type, ENGINE *impl);
int EVP_DigestUpdate(EVP_MD_CTX *ctx, const unsigned char *in, int inl);
int EVP_DigestFinal_ex(EVP_MD_CTX *ctx, unsigned char *md,
        unsigned int *s);

int EVP_DigestInit(EVP_MD_CTX *ctx, const EVP_MD *type);
int EVP_DigestFinal(EVP_MD_CTX *ctx, unsigned char *md,
        unsigned int *s);

int EVP_SignFinal(EVP_MD_CTX *ctx,unsigned char *sig,unsigned int *s,
                  EVP_PKEY *pkey);
int EVP_VerifyFinal(EVP_MD_CTX *ctx,unsigned char *sigbuf, unsigned int siglen,
                    EVP_PKEY *pkey);
int EVP_PKEY_set1_RSA(EVP_PKEY *e, RSA *r);

void ERR_set_error_data(char *data, int flags);
]]

local ERR_TXT_STRING = 0x02
local EVP_MAX_MD_SIZE = 64

local function crypto_err()
    local err_queue = {}
    local i = 1
    local data = ffi.new("const char*[1]")
    local flags = ffi.new("int[1]")

    while true do
        local code = fii.C.ERR_get_error_line_data(nil, nil, data, flags)
        if code == 0 then
            break
        end

        local err = crypto.ERR_reason_error_string(code)
        err_queue[i] = ffi.string(err)
        i = i + 1

        if data[0] ~= nil and bit.band(flags[0], ERR_TXT_STRING) > 0 then
            err_queue[i] = ffi.string(data[0])
            i = i + 1
        end
    end

    error(table.concat(err_queue, ": ", 1, i - 1))
end

local function digest(algorithm, str)
  local md = crypto.EVP_get_digestbyname(algorithm)
  if md == nil then
    error("Unknown message digest")
  end

  local ctx = ffi.gc(crypto.EVP_MD_CTX_new(), crypto.EVP_MD_CTX_free)

  if crypto.EVP_DigestInit_ex(ctx, md, nil) <= 0 then
    crypto_error()
  end

  if crypto.EVP_DigestUpdate(ctx, str, #str) <= 0 then
    crypto_error()
  end

  local buf = ffi.new("unsigned char[?]", EVP_MAX_MD_SIZE)
  local len = ffi.new("unsigned int[1]")
  if crypto.EVP_DigestFinal_ex(ctx, buf, len) <= 0 then
    crypto_error()
  end
  return ffi.string(buf, len[0])
end

return {
  digest = digest,
}
