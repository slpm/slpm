#!/usr/bin/env luajit

local api = require "api"
local libc = require "src.luajit.libc"
require "src.lua.text".format_operator()

local function exec(cmd, target, no_print)
  local locals = {
    LUAJIT = os.getenv("LUAJIT") or "luajit",
    TARGET = target,
    ZIG = os.getenv("ZIG") or "zig",
  }
  local output = api.private.exec(cmd % locals, not no_print)
  if no_print then
    return output
  else
    api.private.eprint(output)
  end
end

local suite = arg[1]

if not suite or suite == "target-info" then
  exec("$ZIG build --build-file tools/target_info/build.zig --help")
  exec("$LUAJIT ./slpm.lua run tools/target_info")
end

if not suite or suite == "lua" then
  local lua_targets = {
    "x86_64-linux-musl",
    "aarch64-linux-musl",
    "arm-linux-musleabi",
    "x86_64-windows-gnu",
    "x86_64-macos-gnu"
  }

  exec("$ZIG build --build-file modules/native/lua/build.zig --help")

  -- Lua for ARM doesn't pass sanitizers enabled in default debug mode
  for _, target in ipairs(lua_targets) do
    exec("$LUAJIT ./slpm.lua test -m small -t $TARGET modules/native/lua", target)
  end

  exec("$ZIG build --build-file tools/lua_host/build.zig --help")

  for _, target in ipairs(lua_targets) do
    exec("$LUAJIT ./slpm.lua run -m small -t $TARGET tools/lua_host -- test/test.lua test/primes.lua", target)
  end

  -- Run using -q option
  local meta = exec("$LUAJIT ./slpm.lua build -q tools/lua_host", "", true)
  local exe = api.public.json_decode(meta)[1]['bin']['main']['object']
  exec(exe)
end

if not suite or suite == "luajit" then
  local luajit_targets = {
    "x86_64-linux-musl",
    "aarch64-linux-musl",
    "arm-linux-musleabi",
    "x86_64-windows-gnu",
  }

  exec("$ZIG build --build-file modules/native/luajit/build.zig --help")

  -- LuaJIT doesn't pass sanitizers enabled in default debug mode
  for _, target in ipairs(luajit_targets) do
    exec("$LUAJIT ./slpm.lua test -m small -t $TARGET modules/native/luajit", target)
  end

  exec("$ZIG build --build-file tools/luajit_host/build.zig --help")

  for _, target in ipairs(luajit_targets) do
    exec("$LUAJIT ./slpm.lua run -m small -t $TARGET tools/luajit_host -- test/test.lua", target)
  end
end

if not suite or suite == "openssl" then
  local openssl_targets = {
    "x86_64-linux-musl",
    "aarch64-linux-musl",
    "arm-linux-musleabi",
    "x86_64-macos-gnu",
    "x86_64-windows-gnu",
  }

  exec("$ZIG build --build-file modules/native/openssl/build.zig --help")

  for _, target in ipairs(openssl_targets) do
    exec("$LUAJIT ./slpm.lua test -t $TARGET modules/native/openssl", target)
  end
end

if not suite or suite == "curl" then
  local curl_targets = {
    "x86_64-linux-musl",
    "aarch64-linux-musl",
    "arm-linux-musleabi",
    "x86_64-macos-gnu",
    "x86_64-windows-gnu",
  }

  exec("$ZIG build --build-file modules/native/curl/build.zig --help")

  for _, target in ipairs(curl_targets) do
    exec("$LUAJIT ./slpm.lua test -t $TARGET modules/native/curl", target)
  end
end

if not suite or suite == "procstat" then
  local procstat_targets = {
    "x86_64-linux-musl",
    "aarch64-linux-musl",
    "arm-linux-musleabi",
    "x86_64-windows-gnu",
    "x86_64-macos-gnu"
  }

  exec("$ZIG build --build-file modules/native/procstat/build.zig --help")

  for _, target in ipairs(procstat_targets) do
    exec("$LUAJIT ./slpm.lua test -t $TARGET modules/native/procstat", target)
  end
end

if not suite or suite == "example-curl" then
  local curl_targets = {
    "x86_64-linux-musl",
    "aarch64-linux-musl",
    "arm-linux-musleabi",
    "x86_64-windows-gnu",
    "x86_64-macos-gnu",
  }

  exec("$ZIG build --build-file examples/curl/build.zig --help")

  -- Curl for macOS doesn't pass sanitizers enabled in default debug mode
  for _, target in ipairs(curl_targets) do
    exec("$LUAJIT ./slpm.lua run -t $TARGET -m small examples/curl -- http://example.com", target)
  end
end
