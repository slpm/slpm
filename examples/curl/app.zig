const std = @import("std");
const c = @cImport({
    @cInclude("curl/curl.h");
});

const assert = std.debug.assert;

const Allocator = std.mem.Allocator;
const Str = []const u8;

extern fn set_curlopt_url(handle: ?*c.CURL, url: [*c]const u8) c.CURLcode;

fn curl_assert(code: c.CURLcode) void {
    if (code != c.CURLE_OK) {
        std.debug.print("{s}\n", .{c.curl_easy_strerror(code)});
        std.os.exit(1);
    }
}

fn run(allocator: *Allocator, url: Str) !void {
    const h = c.curl_easy_init();
    if (h != null) {
        defer c.curl_easy_cleanup(h);

        const c_url = try std.cstr.addNullByte(allocator, url);
        curl_assert(set_curlopt_url(h, c_url));
        curl_assert(c.curl_easy_perform(h));
    }
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = &arena.allocator;

    var args_it = std.process.args();
    assert(args_it.skip());

    while (args_it.next(allocator)) |arg| {
        try run(allocator, try arg);
    }
}
