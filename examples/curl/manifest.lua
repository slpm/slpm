local build

name = "curl"
description = "curl example"

versions = {
  {
    version = "0.0.1",
    build = function(cfg, api)
      return build(cfg, api)
    end,
    dependencies = {
      { name = "native/curl", version = "7.79.1" },
    }
  }
}

build = function(cfg, api)
  local bin_main_name = string.format(
    "%s-%s-%s", cfg.pkg.name, cfg.args.mode, cfg.pkg.version)
  local dep_opts = cfg.pkg.dep_opts
  local options = {
    bin = {
      main = {
        name = bin_main_name,
        object = api.path.join(cfg.prefix.bindir, bin_main_name),
      }
    },
    prefix = cfg.prefix,
    dep_opts = dep_opts,
    mode = cfg.args.mode,
    target = cfg.args.target,
  }

  api.zig_build(cfg, string.format("-Doptions=%q", api.json_encode(options)))

  return { bin = options.bin }
end
