#include "curl/curl.h"

CURLcode set_curlopt_url(CURL *handle, const char* url) {
  return curl_easy_setopt(handle, CURLOPT_URL, url);
}
